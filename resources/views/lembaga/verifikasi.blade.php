@extends('layouts.adminlte')

@section('content')
<div class="row">
    <div class="col-md-12">
      <div class="card card-outline card-warning">
        <div class="card-header with-border">
          <h3 class="card-title">{{$title}}</h3>
          
        </div><!-- /.card-header -->
        <div class="card-body">
            <table id="example1" class="table table-bordered">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Nama</th>
                  <th>Alamat</th>
                  <th>Email</th>
                  <th>Telp</th>
                  <th>Lat</th>
                  <th>Lng</th>
                  <th>Verifikasi</th>
                  <th>Opsi</th>
                </tr>
              </thead>
              <tbody>
                @foreach($user as $d)
                <tr>
                  <td>{{$loop->iteration}}</td>
                  <td>{{$d->name}}</td>
                  <td>{{$d->alamat}}</td>
                  <td>{{$d->email}}</td>
                  <td>{{$d->no_telp}}</td>
                  <td>{{$d->lat}}</td>
                  <td>{{$d->lng}}</td>
                  <td>
                    
                    @if($d->verified == 'Y')
                      <i class="fa fa-check-circle bg-success p-1"></i> 
                    @else
                      <i class="fa fa-times-circle-o bg-danger p-1"></i>
                    @endif
                  </td>
                  <td>
                    <a href="#" data-toggle="modal" data-target="#exampleModal<?=$d->id?>" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i></a>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
        </div><!-- /.card-body -->
      </div><!-- /.card -->
    </div>
</div>



<!-- Modal -->
@foreach($user as $d)
<div class="modal fade" id="exampleModal<?=$d->id?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLabel">Verifikasi</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{route('admin.editverifikasi')}}" method="post">
        @csrf
        <input type="hidden" name="id" value="{{$d->id}}">
      <div class="modal-body">
        <select class="form-control" name='verifikasi' id="verifikasi">
          <option value="">Pilih</option>
          <option value="Y" <?=$d->verified == 'Y' ? 'selected' : ''?> >Yes</option>
          <option value="N" <?=$d->verified == 'N' ? 'selected' : ''?>>No</option>
        </select>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form>
    </div>
  </div>
</div>
 @endforeach

@endsection


@section('js')
<script type="text/javascript">
  
  $("#example1").DataTable({
  "paging": true,
  "lengthChange": false,
  "searching": true,
  "ordering": true,
  "info": true,
  "autoWidth": false,
  "responsive": true,
});


</script>

@endsection
