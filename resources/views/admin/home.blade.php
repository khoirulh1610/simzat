@extends('layouts.adminlte')

@section('content')
<div class="content">
    <!-- <div class="alert alert-warning alert-dismissable">
        Selamat datang <B>{{Auth::user()->name}}</B>, anda login berhasil.
        <ul>
            <li>1. Akses sebagai admin</li>
            <li>2. Anda memiliki akses penuh terhadap sistem</li>
            <li>3. Logout sistem selesai pemakaian</li>
        </ul>

    </div> -->
    <div class="row">
        <div class="col-md-12">
            <table class="table" id="tbuser">
                <thead>
                    <th>Name</th>
                    <th>Email</th>
                    <th>PMA</th>
                    <th>Website</th>
                    <th>Type</th>
                    <th>Map</th>
                </thead>
                <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>{{$user->name}}</td>                            
                            <td>{{$user->email}}</td>
                            <td>{{$user->tgl_pma}}</td>
                            <td>{{$user->website}}</td>
                            <td>{{$user->user_type}}</td>
                            <td>
                                <?php
                                    $loc = "https://www.google.com/maps/search/?api=1&query=".$user->lat.",".$user->lng;                                    
                                    if($user->lat && $user->lng){
                                        echo "<a href='$loc' target='_blank'><i class='fa fa-globe' aria-hidden='true'></i></a>";
                                    }
                                 ?>   
                                 
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>    
    </div>
</div>
@endsection

@section('js')
    <script>    
        $('#tbuser').DataTable();
    </script>    
@endsection