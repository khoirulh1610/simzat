@extends('layouts.adminlte')

@section('content')
<div class="row">
    <div class="col-md-12">
      <div class="card card-outline card-warning">
        <div class="card-header with-border">
          <h3 class="card-title">Pengaduan Masyarakat</h3>
          
        </div><!-- /.card-header -->
        <div class="card-body">
            <table id="example2" class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Nama</th>
                  <th>Email</th>
                  <th>Telp</th>
                  <th>Pengaduan</th>
                  <th>Tanggal</th>
                </tr>
              </thead>
              <tbody>
                @foreach($pengaduan as $data)
                <tr>
                  <td>{{$loop->iteration}}</td>
                  <td>{{$data->nama}}</td>
                  <td>{{$data->email}}</td>
                  <td>{{$data->no_hp}}</td>
                  <td>{{$data->keterangan}}</td>
                  <td>{{$data->created_at}}</td>
                </tr>
                @endforeach
              </tbody>
            </table>

              
        </div><!-- /.card-body -->
      </div><!-- /.card -->
    </div>
</div>
@endsection


@section('js')
<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true,
      "autoWidth": false,
    });
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>
@endsection
