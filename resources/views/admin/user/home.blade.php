@extends('layouts.adminlte')

@section('content')
<div class="row">
    <div class="col-md-12">
      <div class="card card-outline card-warning">
        <div class="card-header with-border">
          <h3 class="card-title">Master User</h3>
          <div class="card-tools">
            <button type="button" class="btn btn-sm btn-info" onclick="newentry();"><i class="fa fa-plus-circle"> Tambah</i>
            </button>
          </div>
        </div><!-- /.card-header -->
        <div class="card-body table-responsive">
            <i id="progress" name="progress"></i>
            <table id="datatable" class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Nama</th>
                  <th>Email</th>
                  <th>Type</th>
                  <th>Telp</th>
                  <th>Status</th>
                  <th>Lat</th>
                  <th>Lng</th>
                  <th>Opsi</th>
                </tr>
              </thead>
              <tbody>
                  @foreach($data as $tampil)
                  <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$tampil->name}}</td>
                    <td>{{$tampil->email}}</td>
                    <td>{{$tampil->user_type}}</td>
                    <td>{{$tampil->no_hp}}</td>
                    <td>{{$tampil->status}}</td>
                    <td>{{$tampil->lat}}</td>
                    <td>{{$tampil->lng}}</td>
                    <td>
                      <?php
                        echo "<a href='#' class='btn btn-xs btn-success' onClick='showakses($tampil->id)'><i class='fa fa-key'></i></a> 
                        <a href='#' class='btn btn-xs btn-info' onClick='showedit($tampil->id)'><i class='fa fa-edit'></i></a>
                        <a href='#' class='btn btn-xs btn-danger' onClick='showdel($tampil->id)'><i class='fa fa-trash'></i></a>";
                      ?>
                    </td>
                  </tr>
                  @endforeach
              </tbody>
            </table>
        </div><!-- /.card-body -->
      </div><!-- /.card -->
    </div>
</div>



<!-- Modal -->
<div class="modal fade" id="modalcrud" tabindex="-1" aria-labelledby="modalcrudLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content ">
      <div class="modal-header">
        <h5 class="modal-title" id="modalcrudLabel">Entry Data</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <input type="hidden" name="id" id="id" value="0">
          <div class="col-md-6">
            <label for="">Nama Lengkap :</label>
            <input type="text" name="nama" id="nama" class="form-control"/>
          </div>
          
          <div class="col-md-6 ml-auto">
            <label>Email :</label>
            <input type="email" name="email" id="email" class="form-control"/>
          </div>
          
          <div class="col-md-12 ml-auto">
            <label>Alamat :</label>
            <input type="text" name="alamat" id="alamat" class="form-control"/>
          </div>
          
          <div class="col-md-6 ml-auto">
            <label>Website :</label>
            <input type="text" name="website" id="website" class="form-control"/>
          </div>

          <div class="col-md-6 ml-auto">
            <label>Password :</label>
            <input type="password" name="password" id="password" class="form-control"/>
          </div>

          <div class="col-md-4 ml-auto">
            <label>Telp :</label>
            <input type="text" name="telp" id="telp" class="form-control"/>
          </div>

          <div class="col-md-4 ml-auto">
            <label>Type :</label>
            <select name="type" id="type" class="form-control" required>
              <option value="">-Pilih-</option>
              <option value="Admin">Admin</option>
              <option value="BAZ">BAZ</option>
              <option value="LAZ">LAZ</option>
              <option value="BBI">BBI</option>
              <option value="AKR">AKR</option>
              <option value="LEM">LEM</option>
            </select>
          </div>

          <div class="col-md-4 ml-auto">
            <label>Status :</label>
            <select name="status" id="status" class="form-control" required>
              <option value="">-Pilih-</option>
              <option value="active">Aktif</option>
              <option value="non active">Tidak Aktif</option>
            </select>
          </div>

          <div class="col-md-6 ml-auto">
            <label>Lat :</label>
            <input type="text" name="lat" id="lat" class="form-control"/>
          </div>

          <div class="col-md-6 ">
            <label>Long :</label>
            <input type="text" name="lng" id="lng" class="form-control"/>
          </div>
          
        </div>
      </div>
      <div class="modal-footer">
         <input type="hidden" name="act" id="act" value="new">
        <input onclick="saveData()" type="button" class="btn btn-success" value="Simpan Data">
      </div>
    </div>
  </div>
</div>




<!-- modal akses -->
<!-- Modal -->
<div class="modal fade" id="aksesmodal" tabindex="-1" aria-labelledby="aksesmodalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content ">
      <div class="modal-header">
        <h5 class="modal-title" id="aksesmodalLabel">Akses Users</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-4">
            <div class="form-group">
              <label for="">Group Name</label>
              <input type="hidden" id="user_id" name="user_id" class="form-control" value="">
              <input type="hidden" id="group_id" name="group_id" class="form-control" value="">
              <input type="text" id="group" name="group" class="form-control" value="">
            </div>
          </div>
          <div class="col-sm-4">
            <div class="form-group">
              <label for="">Menu</label>
              <select id="menu" name="menu" class="form-control">
                <option value="">-Pilih-</option>
                <option value="LPZ">LPZ</option>
                <option value="Akreditasi & Audit">Akreditasi & Audit</option>
                <option value="Lembaga & Info Zawa">Lembaga & Info Zawa</option>
                <option value="Pengaduan Masyarakat">Pengaduan Masyarakat</option>
                <option value="Modul Users">Modul Users</option>
              </select>
            </div>
          </div>
          <div class="col-sm-4">
            <label for="">&nbsp;</label> <br>
            <button class="btn btn-primary" id="btn_tambahakses">Tambah</button>
          </div>
        </div>
        <table class="table">
          <thead>
            <tr class="bg-success">
              <td>No</td>
              <td>Name</td>
              <td>Type</td>
              <td>Group</td>
              <td>Menu</td>
              <td>#</td>
            </tr>
          </thead>
          <tbody id="aksestb"></tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<!-- akhir modal akses -->


@endsection


@section('js')
<script type="text/javascript">

function loaddata(){
//   $("#progress").html('<img src="http://webzakat.esy.es/assets/images/ajax-loading30.gif">');

//   $.ajax({
//     "url" : "{{route('admin.loaduser')}}",
//     "type":"GET",
//     "success":function(data){
//       document.getElementById('isitb').innerHTML = data;
//       $("#progress").html('');
//     }
//   });
}


$("#datatable").DataTable({
  "paging": true,
  "lengthChange": false,
  "searching": true,
  "ordering": true,
  "info": true,
  "autoWidth": false,
  "responsive": true,
});
loaddata();



function clearItem(){
    $('#nama').val('');
    $('#email').val('');
    $('#telp').val('');
    $('#type').val('');
    $('#status').val('');
    $('#alamat').val('');
    $('#lat').val('');
    $('#lng').val('');
    $('#website').val('');
    $('#password').val('');
    $('#act').val('new');
}


function saveData(){
  var act = $('#act').val();
  var id = $('#id').val();
  var nama = $('#nama').val();
  var email = $('#email').val();
  var telp = $('#telp').val();
  var type = $('#type').val();
  var status = $('#status').val();
  var lat = $('#lat').val();
  var lng = $('#lng').val();
  var alamat = $('#alamat').val();
  var website = $('#website').val();
  var password = $('#password').val();
  
  if(nama == ''){
    swal("Pemberitahuan","Nama tidak boleh kosong","warning");
    nama.focus();
    return false;
  }

  if(email == ''){
    swal("Pemberitahuan","Email tidak boleh kosong","warning");
    email.focus();
    return false;
  }

  if(telp == ''){
    swal("Pemberitahuan","telp tidak boleh kosong","warning");
    telp.focus();
    return false;
  }

  if(status == ''){
    swal("Pemberitahuan","status tidak boleh kosong","warning");
    status.focus();
    return false;
  }

  if(lat == ''){
    swal("Pemberitahuan","lat tidak boleh kosong","warning");
    lat.focus();
    return false;
  }

  if(lng == ''){
    swal("Pemberitahuan","lng tidak boleh kosong","warning");
    lng.focus();
    return false;
  }

  if(alamat == ''){
    swal("Pemberitahuan","Alamat tidak boleh kosong","warning");
    alamat.focus();
    return false;
  }

  if(website == ''){
    swal("Pemberitahuan","Website tidak boleh kosong","warning");
    website.focus();
    return false;
  }


  if(act=='new'){
    act = 'insert';
  }else{
    act = 'update';
  }

  $.ajax({
      "url":"{{url('admin/saveuser')}}",
      "data": {act:act,id:id,nama:nama,email:email,telp:telp,status:status,lat:lat,lng:lng,type:type,alamat:alamat,website:website,password:password,"_token": "{{ csrf_token() }}"},
      "type":"POST",
      "dataType" :"json",
      "success":function(result){

        var hasil = JSON.parse(JSON.stringify(result));
        var cek = hasil.status;
        var msg = hasil.msg;
        if(cek == 'Success'){
          $("#progress").html('');
          $('#modalcrud').modal('hide');
          clearItem();
          loaddata();
          swal("Info",msg,"success");
          location.reload();
        }else{
          swal("Pemberitahuan","Email sudah terdaftar","warning");
        }
      }
  });
}

function showdel(id) {
    var r = confirm("Are you sure you want to delete this Record?");
    if (r == true) {
        $("#progress").html('<img src="http://webzakat.esy.es/assets/images/ajax-loading30.gif">');
        $.ajax({
          "url":"{{url('admin/deleteuser')}}/"+ id,
          "type": "GET",
          "dataType" : "json",
          "success":function(result){
            var hasil = JSON.parse(JSON.stringify(result));
            var cek = hasil.status;
            if(cek == 'Success'){
              $("#progress").html('');
              loaddata();
              swal("Info","Data telah di hapus","success");
              location.reload();
            }else{
              alert(hasil.msg);
            }
          }
        });
    }
  }



  function showedit(id){
    $("#progress").html('<img src="http://webzakat.esy.es/assets/images/ajax-loading30.gif">');
    $.ajax({
      "url":"{{url('admin/showuser')}}/"+ id,
      "type": "GET",
      "dataType" : "json",
      "success":function(result){
        var hasil = JSON.parse(JSON.stringify(result));
        $('#act').val(hasil.data[0].id);
        $('#id').val(hasil.data[0].id);
        $('#nama').val(hasil.data[0].name);
        $('#email').val(hasil.data[0].email);
        $('#telp').val(hasil.data[0].no_hp);
        $('#type').val(hasil.data[0].user_type);
        $('#status').val(hasil.data[0].status);
        $('#lat').val(hasil.data[0].lat);
        $('#lng').val(hasil.data[0].lng);
        $('#alamat').val(hasil.data[0].alamat);
        $('#website').val(hasil.data[0].website);
        $('#modalcrud').modal('show');
        $("#progress").html('');
      }
    });
  }

  function newentry() {
    clearItem();
    $('#modalcrud').modal('show');
  }


  function showakses(id){
    $("#aksesmodal").modal('show');
    $.ajax({
      "url":"{{url('admin/showgroup')}}/"+id,
      "type":"GET",
      "success":function(result){
        // console.log(result[0].group);
        if(result.length > 0){
         $("#group_id").val(result[0].id);
         $("#group").val(result[0].group);
         $("#user_id").val(id);
        }else{
         $("#group_id").val("");
         $("#group").val("");
         $("#user_id").val(id);
       }
       
       if(result.length > 0){
        var group_idx = result[0].id;
        aksesData(group_idx);
       }else{
        aksesData(0);
       }
      }
    });
  }

  $("#btn_tambahakses").click(function(){
    var group_id = $("#group_id").val();
    var group = $("#group").val();
    var menu = $("#menu").val();
    var user_id = $("#user_id").val();
    if(menu.length == 0){
      alert("Menu harus dipilih");
      return false;
    }
    $.ajax({
      'url' : "{{url('/admin/tambah-akses')}}",
      'type' : 'POST',
      'data' : {group_id:group_id, group:group, menu:menu, user_id:user_id,"_token": "{{ csrf_token() }}"},
      'success' : function(data){
        // location.reload();
        group_id = $("#group_id").val();
        if(group_id.length == 0){
          alert(data);
          location.reload();
        }else{
          hasil = JSON.parse(data);
          alert("Data telah di simpan");
          aksesData(data);
          console.log(data);  
        }
      }
    });
  });

  function aksesData(id) {
    $.ajax({
      "url": "{{url('admin/akses-data')}}/"+id,
      "type":"GET",
      "success":function(data){
        document.getElementById('aksestb').innerHTML = data;
      }
    });
  }


  function aksesHapus(id) {
    var r = confirm('Apakah anda yakin hapus ?');
    if(r == true){
      $.ajax({
        "url": "{{url('admin/akses-hapus')}}/"+id,
        "type":"GET",
        "success":function(data){
          // location.reload();
          aksesData(data);
          // console.log(data);
        }
      });  
    }
  }
</script>
@endsection
