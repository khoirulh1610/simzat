<!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-light navbar-success">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button" ><i class="fas fa-bars" style="color:#fff"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a class="nav-link"  style="color:#fff;font-size:16px">Login : {{ucwords(Auth::user()->user_type)}}</a>
      </li>
     <!--  <li class="nav-item d-none d-sm-inline-block">
        <a href="{{$path_asset}}index3.html" class="nav-link"  style="color:#fff">Home</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link" style="color:#fff">Contact</a>
      </li> -->
    </ul>

    

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Messages Dropdown Menu -->
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar elevation-4 sidebar-light-success">
    <!-- Brand Logo -->
    <a href="{{$path_asset}}index3.html" class="brand-link navbar-success">
      <img src="{{asset('mobapp/images/logo-depag.png')}}"
           alt="AdminLTE Logo"
           class="brand-image"
           style="opacity: .8">
      <span class="brand-text" style="color:#fff;font-size: 16px">LEMBAGA AMIL ZAKAT</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{asset('images/profil.jpg')}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{ Auth::user()->name }}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item ">
            <a href="{{route('admin.home')}}" class="nav-link {{ url('/admin/home') == request()->url() ? 'active' : '' }}">
              <i class="nav-icon fas fa-home"></i>
              <p>Beranda</p>
            </a>
          </li>

          @if(Auth::user()->user_type == 'admin' || Auth::user()->user_type == 'LAZ' || Auth::user()->user_type == 'BBI' || Auth::user()->user_type == 'BAZ')
          <li class="nav-item has-treeview {{ url('/admin/kandidat/profil') == request()->url() || url('/admin/kandidat/document') == request()->url() || url('/admin/kandidat/getdist') == request()->url() ?  'menu-open' : '' }}">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-list-alt"></i>
              <p>
                LPZ
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('admin.profil')}}" class="nav-link {{ url('/admin/kandidat/profil') == request()->url() ? 'active' : '' }}">
                  <i class="fas fa-check-circle nav-icon"></i>
                  <p>Profile</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('admin.document')}}" class="nav-link {{ url('/admin/kandidat/document') == request()->url() ? 'active' : '' }}">
                  <i class="fas fa-check-circle nav-icon"></i>
                  <p>Upload Document</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('admin.getdist')}}" class="nav-link {{ url('/admin/kandidat/getdist') == request()->url() ? 'active' : '' }}">
                  <i class="fas fa-check-circle nav-icon"></i>                  
                  <p>Perolehan & Distribusi</p>
                </a>
              </li>
            </ul>
          </li>
          @endif


          @if(Auth::user()->user_type == 'admin' || Auth::user()->user_type == 'AKR')
          <li class="nav-item has-treeview {{ url('/admin/audit/import') == request()->url() || 
                            url('/admin/audit/kinerja') == request()->url() ||
                            url('/admin/audit/kepatuhan') == request()->url() || 
                            Request::segment(3) == 'kinerja_nilai' ||
                            Request::segment(3) == 'kepatuhan_nilai' ?  'menu-open' : '' }}">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-list-alt"></i>
              <p>
                Akreditasi & Audit
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview ">
              <li class="nav-item">
                <a href="{{route('admin.import')}}" class="nav-link {{ url('/admin/audit/import') == request()->url() ? 'active' : '' }}">
                  <i class="fas fa-check-circle nav-icon"></i>
                  <p>Import Data LPZ</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('admin.kinerja')}}" class="nav-link {{ url('/admin/audit/kinerja') == request()->url() || 
                                  Request::segment(3) == 'kinerja_nilai' ? 'active' : '' }}">
                  <i class="fas fa-check-circle nav-icon"></i>
                  <p>Penilaian Akreditasi</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('admin.kepatuhan')}}" class="nav-link {{ url('/admin/audit/kepatuhan') == request()->url() || 
                                  Request::segment(3) == 'kepatuhan_nilai'? 'active' : '' }}">
                  <i class="fas fa-check-circle nav-icon"></i>
                  <p>Kepatuhan Syariah</p>
                </a>
              </li>
            </ul>
          </li>
          @endif

          @if(Auth::user()->user_type == 'admin' || Auth::user()->user_type == 'LEM')
          <li class="nav-item has-treeview {{ url('/admin/lembaga/verifikasi') == request()->url() ?  'menu-open' : '' }}">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-list-alt"></i>
              <p>
                Lembaga & Info Zawa
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('admin.verifikasi')}}" class="nav-link {{ url('/admin/lembaga/verifikasi') == request()->url() ? 'active' : '' }}">
                  <i class="fas fa-check-circle nav-icon"></i>
                  <p>Verifikasi</p>
                </a>
              </li>
            </ul>
          </li>
          @endif

          @if(Auth::user()->user_type == 'admin')
          <li class="nav-item ">
            <a href="{{route('admin.aduan')}}" class="nav-link {{ url('/admin/aduan') == request()->url() ? 'active' : '' }}">
              <i class="nav-icon fas fa-list-alt"></i>
              <p>Pengaduan Masyarakat</p>
            </a>
          </li>
          <li class="nav-item ">
            <a href="{{route('admin.user')}}" class="nav-link {{ url('/admin/user') == request()->url() ? 'active' : '' }}">
              <i class="nav-icon fas fa-list-alt"></i>
              <p>Modul Users</p>
            </a>
          </li>
          @endif  


          <li class="nav-item">
            <a href="{{ route('logout') }}" class="nav-link" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
              <i class="nav-icon fas fa-power-off"></i>
              <p>Logout</p>
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>