<?php
  $path_asset = asset('bower_components/adminlte').'/';
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Halaman Admin - Sistem Informasi Zakat</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="shortcut icon" href="{{asset('mobapp/images/logo-depag.png')}}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{$path_asset}}plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="{{$path_asset}}plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="{{$path_asset}}plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{$path_asset}}dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <!-- FontAwesome 4.3.0 -->
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
  <!-- Ionicons 2.0.0 -->
  <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />    


  <!-- <link rel="stylesheet" type="text/css" href="{{url('css/daterangepicker.css')}}" />
  <script type="text/javascript" src="http://zakat.smartcorp.co.id/assets/js/daterangepicker.js"></script> -->

  <!-- DataTables -->
  <link rel="stylesheet" href="{{$path_asset}}plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="{{$path_asset}}plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <style type="text/css"> 
      .navbar-success {
        background-color: rgba(3, 89, 11);
      }

      .btn-success{
        background-color: rgba(3, 89, 11);
      }
    </style>
</head>
<body class="hold-transition sidebar-mini layout-fixed ">
<div class="wrapper">
  @include('layouts.menu')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{$title}}</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">{{$title}}</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
          @yield('content')
      </div>
      
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <!-- <div class="float-right d-none d-sm-block">
      <b>Version</b> 3.0.5
    </div> -->
    <strong>Copyright &copy; 2020 Kementrian Agama Republik Indonesia
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<!-- <script src="{{$path_asset}}plugins/jquery/jquery.min.js"></script> -->
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
<!-- Bootstrap 4 -->
<script src="{{$path_asset}}plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables -->
<script src="{{$path_asset}}plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{{$path_asset}}plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="{{$path_asset}}plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="{{$path_asset}}plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<!-- AdminLTE App -->
<script src="{{$path_asset}}dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{$path_asset}}dist/js/demo.js"></script>

<script type="text/javascript" src="{{url('js/daterangepicker.js')}}"></script>


@yield('js')
</body>
</html>
