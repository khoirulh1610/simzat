<!doctype html>
<html lang="en">
<!--

Page    : index / MobApp
Version : 1.0
Author  : Colorlib
URI     : https://colorlib.com

 -->

<head>
    <title>SIMZAT - Sistem Informasi Zakat</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Mobland - Mobile App Landing Page Template">
    <meta name="keywords" content="HTML5, bootstrap, mobile, app, landing, ios, android, responsive">

    <link rel="shortcut icon" href="{{asset('mobapp/images/logo-depag.png')}}">
    <!-- Font -->
    <link rel="dns-prefetch" href="//fonts.googleapis.com">
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('mobapp/css/bootstrap.min.css')}}">
    <!-- Themify Icons -->
    <link rel="stylesheet" href="{{asset('mobapp/css/themify-icons.css')}}">
    <!-- Owl carousel -->
    <link rel="stylesheet" href="{{asset('mobapp/css/owl.carousel.min.css')}}">
    <!-- Main css -->
    <link href="{{asset('mobapp/css/style.css')}}" rel="stylesheet">
    <!-- my css -->
    <link href="{{asset('mobapp/css/mystyle4.css')}}" rel="stylesheet">
    <!-- maps -->
    <script src="http://maps.googleapis.com/maps/api/js"></script>

    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <script>
        <?php
            // $con = DB::connection('mysql');
            // $users = $con->select("select *from users where `name` is not null and `lat` is not null and lng is not null and user_type in ('LAZ','BBI','BAZ')");
            $users = \DB::table('users')
            ->whereNotNull('name')        
            ->whereNotNull('lat')        
            ->whereNotNull('lng')   
            ->where('user_type','=','BBI')              
            ->orwhere('user_type','=','LAZ')              
            ->orwhere('user_type','=','BAZ')              
            ->get();
            
        ?>
        
         var markers = [
             @foreach($users as $user)
                ['{{$user->name}}', {{$user->lat}} , {{$user->lng}}],
             @endforeach
            
            
        ];
 
      function initialize() {
        var mapCanvas = document.getElementById('googleMap');
        var mapOptions = {
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }     
        var map = new google.maps.Map(mapCanvas, mapOptions)
 
        var infowindow = new google.maps.InfoWindow(), marker, i;
        var bounds = new google.maps.LatLngBounds(); // diluar looping
        for (i = 0; i < markers.length; i++) {  
        pos = new google.maps.LatLng(markers[i][1], markers[i][2]);
        bounds.extend(pos); // di dalam looping
        marker = new google.maps.Marker({
            position: pos,
            map: map,            
        });
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                infowindow.setContent(markers[i][0]);
                infowindow.open(map, marker);
            }
        })(marker, i));        
        map.fitBounds(bounds); // setelah looping
        }
    
        }
    
    
        google.maps.event.addDomListener(window, 'load', initialize);
    </script>
</head>


<style type="text/css">
  


  
</style>

<body data-spy="scroll" data-target="#navbar" data-offset="30">
       
    <!-- Nav Menu -->

    <div class="nav-menu fixed-top">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <nav class="navbar navbar-dark navbar-expand-lg">
                        <a class="navbar-brand" href="{{url('')}}"><img src="{{asset('mobapp/images/logo12.png')}}" class="img-fluid" alt="logo"></a> <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
                        <div class="collapse navbar-collapse" id="navbar">
                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item"> <a class="nav-link active" href="#home" style="color:#fff;">BERANDA <span class="sr-only">(current)</span></a> </li>
                                <li class="nav-item"> <a class="nav-link" href="#peta" style="color:#fff;">PETA</a> </li>
                                <li class="nav-item"> <a class="nav-link" href="#pengaduan" style="color:#fff;">PENGADUAN MASYARAKAT</a> </li>
                                <li class="nav-item"><a href="{{url('login')}}" class="btn btn-outline-light my-3 my-sm-0 ml-lg-3" style="color:#fff;">Login</a></li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>


    <header class="bg-gradient" id="home">
        <div class="container mt-5">
            <div class="row"></div>
            <form action="" method="get">
                <input type="hidden" id="tab" name="tab" value="{{$tab}}">
               <!--  <input type="" class="form-control input-lg" name="filter" id="filter" placeholder="Pencarian" value="{{$filter ?? ''}}"> -->
               <div class="input-group">
                    <input type="text" class="form-control" name="filter" id="filter" placeholder="Pencarian" value="{{$filter ?? ''}}">
                    <button class="input-group-addon"><i class="fa fa-search"></i></button>
                  </div>
            </form>
        </div>
        
        <style>
            /* not active */
            .nav-link:not(.active) {
                color: white;
            }
            /* active (faded) */
            .nav-link {                
                color: yellow;
            }
            .nav > li > a:hover{
                color:yellow;
            }
            .nav > li > a:focus{
                color:yellow;
            }
            hr{
                border: 1px solid green;
                border-radius: 5px;
            }
        </style>
        <div class="img-holder mt-3 ml-5 mr-5 text-center">
            <!-- Custom Tabs -->
            
            <div class="container mt-0 ml-5">
                <!-- Nav tabs -->
                <ul class="nav" role="tablist" >
                    <li class="nav-item bg-gradient">
                        <a class="nav-link {{$tab=='tab1' ? 'active' : ''}}" onclick="updatetab('tab1')" data-toggle="tab" href="#menu1">BAZNAS</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{$tab=='tab2' ? 'active' : ''}}"  onclick="updatetab('tab2')" data-toggle="tab" href="#menu2">LAZ</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{$tab=='tab3' ? 'active' : ''}}"  onclick="updatetab('tab3')" data-toggle="tab" href="#menu3">Lain nya</a>
                    </li>
                </ul>
                <script>
                    function updatetab(tabval) {
                        $('#tab').val(tabval);
                    }
                </script>
                <!-- Tab panes -->
                <div class="tab-content bg-gradient text-left" style="margin-top:-50px;margin-left:-50px">
                    <div id="menu1" class="container tab-pane {{$tab=='tab1' ? 'active' : 'fade'}} .text-white"><br>
                        @foreach($baznas as $user)
                            <span>{{$user->name}}</span><br>
                            <span>Alamat : {{$user->alamat}}</span><br>
                            <span>No Telp. : {{$user->no_telp}}</span><br>
                            <span>Status : {{$user->status}}</span><br>
                            <hr>
                        @endforeach
                        {{$baznas->links()}}
                    </div>

                    <div id="menu2" class="container tab-pane {{$tab=='tab2' ? 'active' : 'fade'}}"><br>
                        @foreach($laz as $user)
                            <span>{{$user->name}}</span><br>
                            <span>Alamat : {{$user->alamat}}</span><br>
                            <span>No Telp. : {{$user->no_telp}}</span><br>
                            <span>Status : {{$user->status}}</span><br>
                            <hr>
                        @endforeach
                        {{$laz->links()}}
                    </div>
                    <div id="menu3" class="container tab-pane {{$tab=='tab3' ? 'active' : 'fade'}}"><br>
                        @foreach($lain as $user)
                            <span>{{$user->name}}</span><br>
                            <span>Alamat : {{$user->alamat}}</span><br>
                            <span>No Telp. : {{$user->no_telp}}</span><br>
                            <span>Status : {{$user->status}}</span><br>
                            <hr>
                        @endforeach
                        {{$lain->links()}}
                    </div>
                </div>
            </div>
        </div>
    </header>

    <div class="client-logos my-5">
        <div class="container text-center">
            
            <!-- <img src="{{asset('mobapp/images/client-logos.png')}}" alt="client logos" class="img-fluid"> -->
        </div>
    </div>

    <div class="section light-bg" id="peta">


        <div class="container">

            <div class="section-title">
                <small style="color:#03590b">PETA WILAYAH</small>
                <h3>PETA</h3>
            </div>


            <div class="row">
                <div class="col-12 col-lg-12">
                    <div id="googleMap" style="width:100%;height:380px;"></div>
                </div>
            </div>

        </div>

    </div>
    <!-- // end .section -->
 



    <div class="section" id="pengaduan">
        <div class="container">
            <div class="section-title">
                <small style="color:#03590b">PENGADUAN MASYARAKAT</small>
                <h3>Pengaduan Masyarakat</h3>
            </div>

            <div class="row pt-4">
                  
                    <div class="col-md-6">
                        <form action="{{route('aduan.store')}}" method="POST" autocomplete="off" >
                        @csrf
                        <div class="form-group">
                            <label>Nama</label>
                            <input class="form-control @error('nama') is-invalid @enderror" type="text" name="nama" id="nama" value="{{ old('nama') }}" placeholder="Nama Lengkap"> @error('nama')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror

                        </div>

                       <div class="form-group">
                           <label>Email</label>
                           <input class="form-control @error('email') is-invalid @enderror" type="email" name="email" id="email" value="{{ old('email') }}" placeholder="Email"> @error('email')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                       </div>

                       <div class="form-group">
                           <label>No Handphone</label>
                           <input class="form-control @error('no_hp') is-invalid @enderror" type="text" name="no_hp" id="no_hp" value="{{ old('no_hp') }}" placeholder="No Handphone"> @error('no_hp')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                       </div>

                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                           <label>Keterangan</label>
                           <textarea class="form-control @error('keterangan') is-invalid @enderror" rows="5" placeholder="Keterangan" name="keterangan" >{{ old('keterangan') }}</textarea>@error('keterangan')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                       </div>

                       <button type="submit" class="btn btn-success btn-lg btn-block" onClick="return confirm('Apakah anda sudah yakin ?')">Submit</button>
                    </div>
                </form>
            </div>
        </div>

    </div>
    <!-- // end .section -->



    <div class="section bg-gradient-footer">
        <div class="container">

            <div class="row">
                
                <div class="col-sm-4 text-left" >
                    <h3 >Tentang Kami</h3>
                    <p style="color:white">Kami adalah bagian dari Kementrian Agama Republik Indonesia dibawah naungan Direktorat Jendral Bimbingan Masyarakat Islam yang menyajikan seluru informasi mengenai masjid dan mushola di seluruh wilayah indonesia</p>
                </div>

                <div class="col-sm-5 text-left">
                    <h3>Alamat Kami</h3>
                    <p style="color:white">Alamat : Direktorat Jendral Bimbingan    Masyarakat Islam Gedung Kementrian Agama RI Lt.06 <br>
                        Jl. M. H. Thamrin No. 6, jAKARTA 10340 <br> <br>
                        Hunting : (+6221) 3812871 <br>
                        Phone : (+6221) 31924509 - 3193055 - 3920774 <br><br>
                        Email : kemasjidan@kemenag.go.id <br>
                        PO.BOX. 3733 JKP 10037

                    </p>
                </div>

                <div class="col-sm-3 text-left">
                    <h3>Peta Situs</h3>
                    <p style="color:white">
                        
                        Profil Masjid <br> <br>
                        Profil Mushola <br><br>
                        Unduh Data <br><br>
                        Info Terkini <br><br>
                        Kontak Kami

                    </p>
                </div>

            </div>
        </div>

    </div>
    <!-- // end .section -->

    <!-- // end .section -->
    <footer class="my-5 text-center">
        <!-- Copyright removal is not prohibited! -->
        <p class="mb-2"><small>COPYRIGHT © 2020. KEMENTERIAN AGAMA REPUBLIK INDONESIA</small></p>

        <small>
            <a href="#" class="m-2">TENTANG KAMI</a>
            <a href="#" class="m-2">ALAMAT KAMI</a>
            <a href="#" class="m-2">PETA SITUS</a>
        </small>
    </footer>

    <!-- jQuery and Bootstrap -->
    <script src="{{asset('mobapp/js/jquery-3.2.1.min.js')}}"></script>
    <script src="{{asset('mobapp/js/bootstrap.bundle.min.js')}}"></script>
    <!-- Plugins JS -->
    <script src="{{asset('mobapp/js/owl.carousel.min.js')}}"></script>
    <!-- Custom JS -->
    <script src="{{asset('mobapp/js/script.js')}}"></script>

</body>

</html>
