@extends('layouts.adminlte')

@section('content')
<div class="row">
    <div class="col-md-12">
      <div class="card card-outline card-warning">
        <div class="card-header with-border">
          <h3 class="card-title">
            <?php
              $con = DB::connection('mysql');
              $user = $id;
              $data = $con->select("select * from users where user_id like '$user'");
              foreach ($data as $col) {
                echo 'Nama Lembaga : '.$col->name.'';
                echo '<input type="hidden" name="user_id" id="user_id" value="'.$user.'">';
              }
            ?>
          </h3>
          
        </div><!-- /.card-header -->
        <div class="card-body">


          <table id="tb" border="0" style="width:100%">
            <thead style="">
              <tr>
                <td colspan="11"><i>Silanglah dengan tanda (X) pada huruf yang tersedia dibawah ini yang terdiri dari 5 (lima) opsi jawaban yaitu A,B,C,D dan E (A memperoleh skor 4, B memperoleh skor 3, C memperoleh skor 2, D memperoleh skor 1 dan E memperoleh skor 0)</i><br><br></td>
              </tr>
            </thead>
            <tbody id="isitb">


              <?php
              echo '<tr><td colspan="11"></td></tr>';
              echo '<tr><td colspan="11"></td></tr>';
                
                $header = $con->Select("select kin_no,kin_variabel,substr(kin_no,1,LOCATE('.',kin_no)-1) supcat from mst_kinerja WHERE isnull(kin_subno)");
                foreach ($header as $soal) {
                  
                  echo '<tr><td width="20px"><b>'.$soal->supcat.'</b>.</td><td colspan="10"><b>'.$soal->kin_variabel.'</b></td></tr>';
                  
                  
                  $kin_no = $soal->supcat;
                  $data =  $con->select("select kin_no,kin_subno,substr(kin_subno,1,LOCATE('.',kin_subno)-1) supsoal,kin_variabel from mst_kinerja WHERE not isnull(kin_subno) and kin_bobot=0 and substr(kin_no,1,LOCATE('.',kin_no)-1) like '$kin_no'");
                  foreach ($data as $subsoal) {
                    
                    echo '<tr><td width="20px"><u>'.$subsoal->supsoal.'.</u></td><td colspan="10"><u>'.$subsoal->kin_variabel.'</u></td></tr>';

                    $subkinx = $subsoal->kin_no;
                    $xxx =  $con->select("select * from mst_kinerja a left join lpz_kinerja b on (a.kin_no=b.kin_no and a.kin_subno=b.kin_subno and b.user_id='$user') WHERE a.kin_bobot > 0 and a.kin_no = '$subkinx'");
                    foreach ($xxx as $isi) {
                      $r1 = '';
                      $r2 = '';
                      $r3 = '';
                      $r4 = '';
                      $r5 = '';
                      if($isi->kin_val=='A'){
                        $r1 = 'checked';
                      }
                      if($isi->kin_val=='B'){
                        $r2 = 'checked';
                      }
                      if($isi->kin_val=='C'){
                        $r3 = 'checked';
                      }
                      if($isi->kin_val=='D'){
                        $r4 = 'checked';
                      }
                      if($isi->kin_val=='E'){
                        $r5 = 'checked';
                      }
                      echo '<tr><td></td><td width="40px">'.$isi->kin_subno.'</td><td colspan="9">'.$isi->kin_variabel.'</td></tr>';
                      echo '<tr>
                            <td></td><td width="40px">
                            <td colspan="9">
                            <form action="">
                              <input type="radio" '.$r1.' id="'.$isi->kin_no.'" name="'.$isi->kin_subno.'"  onclick="PostNilai(this.id,this.name,4);">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; A &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              <input type="radio" '.$r2.' id="'.$isi->kin_no.'" name="'.$isi->kin_subno.'"  onclick="PostNilai(this.id,this.name,3);">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; B &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              <input type="radio" '.$r3.' id="'.$isi->kin_no.'" name="'.$isi->kin_subno.'"  onclick="PostNilai(this.id,this.name,2);">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; C &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              <input type="radio" '.$r4.' id="'.$isi->kin_no.'" name="'.$isi->kin_subno.'"  onclick="PostNilai(this.id,this.name,1);">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; D &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              <input type="radio" '.$r5.' id="'.$isi->kin_no.'" name="'.$isi->kin_subno.'"  onclick="PostNilai(this.id,this.name,0);">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; E &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </form>
                            <td>
                            </tr>';
                    }

                  }

                  echo '<tr><td colspan="11"></br></td></tr>';
                }
                
                ?>

                <tr><td colspan="9" align="center">AKHIR PENILAIAN</td></tr>
                <tr><td colspan="9" align="center">&nbsp;</td></tr>
                <tr><td colspan="9" align="center">Kumulatif akhir <span id="komulatif">0</span></td></tr>
                <tr><td colspan="9" align="center">Nilai Akreditasi <span id="akreditasi">0</span></td></tr>
            </tbody>
          </table>
            
            


        </div><!-- /.card-body -->
      </div><!-- /.card -->
    </div>
</div>


@endsection


@section('js')
<script type="text/javascript">
  function PostNilai(kin_no,kin_subno,nilai) {
    var user_id = $('#user_id').val();
    $.ajax({
      "url" : "{{url('admin/audit/postkinerja')}}",
      "type" : 'post',
      "data"  : {
          user_id:user_id,
          kin_no:kin_no,
          kin_subno:kin_subno,
          nilai:nilai,
          "_token": "{{ csrf_token() }}"
        },
      "success" : function(){
          calculateNIlai();
      },
      "error":function (er) {
        alert(er);
      }
    });
  }



  function calculateNIlai() {
    var user_id = $('#user_id').val();
    $.ajax({
      "url" : "{{url('admin/audit/nilaikinerja')}}",
      "type" : 'post',
      "data"  : {user_id:user_id,"_token": "{{ csrf_token() }}" },
      "success" : function(data){
        var nil = parseInt(data);
        nil = parseInt(nil/532*100);
          $('#komulatif').html(data);
          $('#akreditasi').html(nil);
      },
      "error":function (er) {
        alert(er);
      }
    });
  }

</script>
@endsection
