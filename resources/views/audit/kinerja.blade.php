@extends('layouts.adminlte')

@section('content')
<div class="row">
    <div class="col-md-12">
      <div class="card card-outline card-warning">
        <div class="card-header with-border">
          <h3 class="card-title">{{$title}}</h3>
          
        </div><!-- /.card-header -->
        <div class="card-body">
            <table class="table table-striped" id="datatable">
              <thead>
                <tr class="bg-warning">
                  <th>No</th>
                  <th>Nama Lembaga</th>
                  <th>Skor</th>
                  <th>Nilai</th>
                  <th>Status</th>
                  <th>Penilaian</th>
                </tr>
              </thead>  
              <tbody>
                <?php
                $con = DB::Connection('mysql');
                $data = $con->select("select a.user_id,name,IFNULL(skor,0) skor,if(ifnull(skor,0)>0,ifnull(skor,0)/532*100,0) nilai,if(penilaian=112,'Full',if(penilaian>0,'Partial','-')) penilaian from users a LEFT JOIN (SELECT user_id,SUM(kin_skor*kin_bobot) skor,SUM(kin_skor*kin_bobot)/532 penilaian,count(kin_skor) qty from lpz_kinerja x LEFT JOIN mst_kinerja y ON (x.kin_no=y.kin_no AND x.kin_subno=y.kin_subno) GROUP BY user_id) b on a.user_id=b.user_id WHERE a.user_type in ('BAZ','BBI','BBI')");
                $n=1;
                foreach ($data as $row) {
                   echo '<tr style="background-color:white">
                    <td>'.$n.'</td>
                    <td>'.$row->name.'</td>
                    <td align="center">'.$row->skor.'</td>
                    <td align="center">'.number_format($row->nilai,0).'</td>
                    <td align="center">'.$row->penilaian.'</td>
                    <td align="center"><a href="'.url('admin/audit/kinerja_nilai/'.$row->user_id).'"  class="btn btn-info"> Pilih</a></td>
                  </tr>';
                  $n++;
                }
                ?>  
              </tbody>
            </table>
        </div><!-- /.card-body -->
      </div><!-- /.card -->
    </div>
</div>


@endsection


@section('js')
<script type="text/javascript">
  $("#datatable").DataTable({
  "paging": true,
  "lengthChange": false,
  "searching": true,
  "ordering": true,
  "info": true,
  "autoWidth": false,
  "responsive": true,
});

</script>

@endsection
