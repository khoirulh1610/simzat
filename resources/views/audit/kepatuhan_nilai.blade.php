@extends('layouts.adminlte')

@section('content')
<div class="row">
    <div class="col-md-12">
      <div class="card card-outline card-warning">
        <div class="card-header with-border">
          <h3 class="card-title">
            <?php
              $con = DB::connection('mysql');
              $user = $id;
              $data = $con->select("select * from users where user_id like '$user'");
              foreach ($data as $col) {
                echo 'Nama Lembaga : '.$col->name.'';
                echo '<input type="hidden" name="pil_user" id="pil_user" value="'.$user.'">';
              }
            ?>  // Monev Kepatuhan Syariah <?=date('Y');?>
          </h3>
          
        </div><!-- /.card-header -->
        <div class="card-body">


          <?php
            $con = DB::connection('mysql');
            $sub = $con->select("select patuh_no from mst_kepatuhan group by patuh_no");
            foreach ($sub as $col) {
              $subno = $col->patuh_no;
              ?>

              <table id="tb" border="1" class="table table-striped">
                <thead>
                  <tr class="bg-warning">
                    <th rowspan="2" align="center" width="40px">No</th>
                    <th rowspan="2" align="center" width="20%">Unsur</th>
                    <th rowspan="2" colspan="2" align="center">Variabel</th>
                    <th rowspan="1" colspan="2" align="center" class="col-md-2">Score</th>
                  </tr>
                  <tr  class="bg-warning">
                    <th align="center" width="10%">Ya/Sebagian/Tidak</th>
                    <th align="center" width="10%">Nilai</th>
                  </tr>
                </thead>
                <tbody id="isitb">

                  <?php
                  $no = 0;
                  $sql = "select a.patuh_no,c.unsur,a.patuh_subno,patuh_variabel,ifnull(patuh_val,'x') patuh_val,patuh_skor,user_id FROM mst_kepatuhan a LEFT JOIN lpz_kepatuhan b ON (a.patuh_subno=b.patuh_subno and b.user_id = '$user') LEFT JOIN (select patuh_no,patuh_variabel as unsur from mst_kepatuhan WHERE patuh_subno=concat(patuh_no,'.0')) c on a.patuh_no=c.patuh_no WHERE a.patuh_subno <> CONCAT(a.patuh_no,'.0') and a.patuh_no = '$subno' order by a.patuh_subno";
                  $data = $con->select($sql);
                  $count = count($data);

                  for($i=0;$i<$count;$i++){
                    if($no!=$data[$i]->patuh_no){
                      echo '<tr><td align="center">'.$data[$i]->patuh_no.'</td><td>'.$data[$i]->unsur.'</td>';
                    }else {
                      echo "<tr><td></td><td></td>";
                    }

                    $a = $i +1;
                    $subno1 = substr($data[$i]->patuh_subno,stripos($data[$i]->patuh_subno,'.')+1,5);
                    $subno3 = '';
                    if(stripos($subno1,".")>0){
                      $subno2 = substr($subno1,0,-stripos($subno1,".")-1);
                      $subno3 = substr($subno1,stripos($subno1,'.')+1,5);
                    }else {
                      $subno2 = $subno1;
                    }
                    if($subno3==''){
                      $var = $subno2;
                    }else {
                      $var = '('.$subno3.')';
                    }

                    $r1 = '';
                    $r2 = '';
                    $r3 = '';
                    if($data[$i]->patuh_val=='2'){
                      $r1 = 'checked';
                      $r2 = '';
                      $r3 = '';
                    }elseif($data[$i]->patuh_val=='1'){
                      $r2 = 'checked';
                      $r1 = '';
                      $r3 = '';
                    }elseif($data[$i]->patuh_val=='0'){
                      $r2 = '';
                      $r3 = '';
                      $r3 = 'checked';
                    }else{
                      $r1 = '';
                      $r2 = '';
                      $r3 = '';
                    }

                    $nila = 0;
                    if($data[$i]->patuh_skor!=''){
                      $nila = $data[$i]->patuh_skor;
                    }
                    echo '<td align="center" width="30px" valign="top">'.$var.'</td>
                          <td valign="top">'.$data[$i]->patuh_variabel.'</td>';
                    if($data[$i]->patuh_subno!='1.5'){
                    echo '<td align="left" valign="top">
                          <form action="">
                              <input type="radio" name="'.$data[$i]->patuh_subno.'" value="Ya" onclick="setnilai(this.name,10,2)" '.$r1.'> Ya <br>
                              <input type="radio" name="'.$data[$i]->patuh_subno.'" value="Sebagian" onclick="setnilai(this.name,5,1)" '.$r2.'> Sebagian <br>
                              <input type="radio" name="'.$data[$i]->patuh_subno.'" value="Tidak" onclick="setnilai(this.name,0,0)" '.$r3.'> Tidak
                          </form>
                          </td>
                          <td align="center"><span id="'.$data[$i]->patuh_subno.'">'.$nila.'</span><input type="hidden" class="'.$subno.'" id="dum_'.$data[$i]->patuh_subno.'" value="0"></td>
                      </tr>';
                    }else {
                      echo '<td align="center" valign="top" colspan="2">
                            </td>
                        </tr>';
                    }

                    $no = $data[$i]->patuh_no;
                  }

                  if($subno==1){
                    echo '<tr class="bg-warning"><td></td><td colspan="4" align="center"><b>Jumlah Total Skor Aspek pengumpulan</b></td><td align="center"><b><span id="nil1">0</span></b></td></tr>';
                  }elseif ($subno==2) {
                    echo '<tr class="bg-warning"><td></td><td colspan="4" align="center"><b>Jumlah Score aspek pendistribusian dan pendayagunaan</b></td><td align="center"><b><span id="nil2">0</span></b></td></tr>';
                  }elseif ($subno==3) {
                    echo '<tr class="bg-warning"><td></td><td colspan="4" align="center"><b>Jumlah Score aspek pengelolaan keuangan</b></td><td align="center"><b><span id="nil3">0</span></b></td></tr>';
                  }
                  ?>
                </tbody>
              </table>
              <?php
            }
          ?>



        </div><!-- /.card-body -->
        <div class="card card-footer">
          <a href="{{route('admin.kepatuhan')}}" class="btn btn-info btn-block">Kembali</a>
          <input type="button" class="btn btn-info btn-block" onclick="relaodAlldata()" value="Refresh">
        </div>
      </div><!-- /.card -->
    </div>
</div>


@endsection


@section('js')
<script type="text/javascript">
 
  function setnilai(id,nil,val) {
    var user_id = $('#pil_user').val();
    $.ajax({
      "url":"{{route('admin.postnilai')}}",
      "type":"post",
      "data":{user_id:user_id,patuh_subno:id,patuh_val:val,patuh_skor:nil,"_token": "{{ csrf_token() }}"},
      "success":function() {
        document.getElementById(id).innerHTML = nil;
        document.getElementById('dum_'+id).value = nil;
        reloadNilai(1);
        reloadNilai(2);
        reloadNilai(3);
      }
    });
  }



  function reloadNilai(x) {
    var user_id = $('#pil_user').val();
    $.ajax({
      "url":"{{route('admin.getsubnilai')}}",
      "type":"post",
      "data":{user_id:user_id,sub:x,"_token": "{{ csrf_token() }}"},
      "success":function(data) {
          $('#nil'+x).html(data);
      }
    });
  }
  function relaodAlldata() {
    reloadNilai(1);
    reloadNilai(2);
    reloadNilai(3);
  }
  $(document).ready(function(){
    relaodAlldata();
  });
</script>
@endsection
