@extends('layouts.adminlte')

@section('content')
<div class="row">
    <div class="col-md-12">
      <div class="card card-outline card-warning">
        <div class="card-header with-border">
          <h3 class="card-title">{{$title}}</h3>
          
        </div><!-- /.card-header -->
        <div class="card-body">
            <table id="datatable" class="table table-striped">
              <thead>
                <tr class="bg-warning">
                  <th>No</th>
                  <th>Nama Lembaga</th>
                  <th>Skor</th>
                  <th>Nilai</th>
                  <th>Status</th>
                  <th>Penilaian</th>
                </tr>
              </thead>  
              <tbody>
                <?php
                $con = DB::Connection('mysql');
                $data = $con->select("select a.user_id,name,IFNULL(skor,0) skor,if(ifnull(skor,0)>0,ifnull(skor,0)/340*100,0) nilai,if(penilaian=34,'Full',if(penilaian>0,'Partial','-')) penilaian from users a LEFT JOIN (select user_id,ifnull(sum(patuh_skor),0) as skor,COUNT(patuh_skor) penilaian from lpz_kepatuhan GROUP BY user_id) b on a.user_id=b.user_id WHERE a.user_type IN ('BAZ','LAZ','BBI')");
                $n=1;
                foreach ($data as $row) {
                   echo '<tr style="background-color:white">
                    <td>'.$n.'</td>
                    <td>'.$row->name.'</td>
                    <td align="center">'.$row->skor.'</td>
                    <td align="center">'.number_format($row->nilai,0).'</td>
                    <td align="center">'.$row->penilaian.'</td>
                    <td align="center"><a href="'.url('admin/audit/kepatuhan_nilai/'.$row->user_id).'"  class="btn btn-info"> Pilih</a></td>
                  </tr>';
                  $n++;
                }
                ?>  
              </tbody>
            </table>
        </div><!-- /.card-body -->
      </div><!-- /.card -->
    </div>
</div>


@endsection


@section('js')
  <script type="text/javascript">
  $("#datatable").DataTable({
  "paging": true,
  "lengthChange": false,
  "searching": true,
  "ordering": true,
  "info": true,
  "autoWidth": false,
  "responsive": true,
});

</script>

@endsection
