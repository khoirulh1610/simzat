@extends('layouts.adminlte')

@section('content')
<div class="row">
    <div class="col-md-12">
      <div class="card card-outline card-warning">
        <div class="card-header with-border">
          <h3 class="card-title">My Account</h3>
          
        </div><!-- /.card-header -->
        <div class="card-body">
            <div class="row">
                <div class="col-md-3 mr-2">
                    <img src="{{asset('images/profil.jpg')}}" class="img-circle elevation-2" alt="User Image" width="100%">
                    <button class="btn  btn-warning btn-block mt-2">Change Picture</button>
                </div>
                <div class="col-md-4">

                    <h3>User Profile</h3>

                    <div class="form-group">
                        <label>Status</label>
                        <select class="form-control">
                            <option value="">Aktif</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" name="" class="form-control" placeholder="Name">
                    </div>

                    <div class="form-group">
                        <label>Status</label>
                        <input type="text" name="" class="form-control" placeholder="Email">
                    </div>

                </div>



                <div class="col-md-4">

                    <h3>Change Password</h3>

                    <div class="form-group">
                        <label>Current Password</label>
                        <input type="text" name="" class="form-control" placeholder="Current Password">
                    </div>

                    <div class="form-group">
                        <label>New Password</label>
                        <input type="text" name="" class="form-control" placeholder="New Password">
                    </div>

                    <div class="form-group">
                        <label>Confirm New Password</label>
                        <input type="text" name="" class="form-control" placeholder="Confirm New Password">
                    </div>

                </div>




                <div class="col-md-12">
                    <div class="form-group">
                        <label>Alamat</label>
                        <textarea class="form-control" placeholder="Alamat"></textarea>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>No. Handphone</label>
                        <input type="text" name="" class="form-control" placeholder="No. Handphone">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>No. Telp</label>
                        <input type="text" name="" class="form-control" placeholder="No. Telp">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Website</label>
                        <input type="text" name="" class="form-control" placeholder="Website">
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Muzakki</label>
                        <input type="text" name="" class="form-control" placeholder="Muzakki">
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Muztahik</label>
                        <input type="text" name="" class="form-control" placeholder="Muztahik">
                    </div>
                </div>

                <div class="col-sm-12">
                    <button class="btn btn-success btn-block">Save Data</button>
                </div>
            </div>
        </div><!-- /.card-body -->
      </div><!-- /.card -->
    </div>
</div>
@endsection
