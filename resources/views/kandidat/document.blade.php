 @extends('layouts.adminlte')

@section('content')
<div class="row">
    <div class="col-md-12">
      <div class="card card-outline card-warning">
        <div class="card-header with-border">
          <h3 class="card-title">Upload Document</h3>
          
        </div><!-- /.card-header -->
        <div class="card-body">
            Pilih Lembaga : 
            <div class="row">
              <div class="col-sm-4">
                <select class="form-control mb-2" name="pilem" id="pilem" onselect="GetTable()">
                  <option value="">-Pilih-</option>
                  @foreach($user as $dt)
                  <option value="{{$dt->id}}">{{$dt->name}}</option>
                  @endforeach
                </select>
              </div>
              <div class="col-sm-6">
                <i id="progress" name="progress"></i>
                <input type="hidden" name="user_id" id="user_id" value="0">
              </div>
            </div>
            <table class="table table-striped">
              <thead>
                <tr class="bg-warning">
                  <th rowspan="2">No</th>
                  <th rowspan="2">Syarat Peserta Akrediasi</th>
                  <th colspan="2">Bukti</th>
                  <th rowspan="2">Document</th>
                </tr>
              </thead>  
              <tbody id="isitb">
                
              </tbody>
            </table>
        </div><!-- /.card-body -->
      </div><!-- /.card -->
    </div>
</div>



<!-- Modal -->
<div class="modal fade" id="uploaddoc" tabindex="-1" aria-labelledby="uploaddocLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="uploaddocLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <input type="hidden" name="syarat" id="syarat" value="">
        <label>
            Browse&hellip; <input type="file" id="filepdf" accept="application/pdf" >
        </label>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" onclick="RunUpload()" class="btn btn-info">Upload</button>
        <span id="linkdw"></span>
      </div>
    </div>
  </div>
</div>
@endsection


@section('js')
<script type="text/javascript">
  
  var events = $('#events');
  function myformatter(date){
    var y = date.getFullYear();
    var m = date.getMonth()+1;
    var d = date.getDate();
    return y+'-'+(m<10?('0'+m):m)+'-'+(d<10?('0'+d):d);
  }

  function myparser(s){
    if (!s) return new Date();
    var ss = (s.split('-'));
    var y = parseInt(ss[0],10);
    var m = parseInt(ss[1],10);
    var d = parseInt(ss[2],10);
    if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
      return new Date(y,m-1,d);
    } else {
      return new Date();
    }
  }

  function loaddata(id) {
    $("#progress").html('<img src="http://webzakat.esy.es/assets/images/ajax-loading30.gif">');
      // $('#callbtn').html('<input type="submit" class="btn btn-info" value="Save" disabled>');
      $.ajax({
        "url":"{{url('admin/lpz/tbsyarat')}}/"+id,
        "type":"GET",
        "success":function(data){
          $('#uploaddoc').modal('hide');
          document.getElementById('isitb').innerHTML = data;
          $("#progress").html('');
        }
      });
  }

  loaddata($('#user_id').val());

  $(function(){
    var selCategory = $('#pilem');
    $(selCategory).change(function() {
      $('option:selected').each(function() {
        loaddata($(this).val());
        $('#user_id').val($(this).val());
      });
    });
  });


  function OpenUpload(val,val2){
    $('#linkdw').html('');
    $('#uploaddoc').modal('show');
    $('#syarat').val(val);
    $('#syarat_detail').html(val2);
    var user_id = $('#user_id').val()
    console.log(user_id);
    var syarat = val;
    $.ajax({
      "url" : "{{url('admin/lpz/url_pdf')}}/"+user_id,
      "data" : {
                  syarat:syarat,
                   "_token": "{{ csrf_token() }}"
                },
      "type" :"post",
      "success":function(data){
        $('#linkdw').html(data);
      }
    });
  }


  function RunUpload(){
    var file_data = $('#filepdf').prop('files')[0];    
    var form_data = new FormData();
    form_data.append('filepdf', file_data);
    form_data.append('user_id', $('#user_id').val());
    form_data.append('syarat', $('#syarat').val());
    $.ajax({
        url: "{{url('admin/lpz/upload_file')}}",
        dataType: 'text',
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        success: function (response) {
            // $('#msg').html(response); // display success response from the server
            // alert(response);
            $('#filepdf').val('');
            loaddata($('#user_id').val());            
        },
        error: function (response) {
            alert(response);
            // $('#msg').html(response); // display error response from the server
        }
    });
  }


  function del_pdf() {
    $.ajax({
      "url": "{{url('admin/lpz/del_pdf')}}",
      "data":{
              syarat:$('#syarat').val(),
              user_id:$('#user_id').val(),
              "_token": "{{ csrf_token() }}"
            },
      "type":"post",
      "success":function(data){
        loaddata($('#user_id').val());
      }
    });

  }
</script>

<style>
#events {
      margin-bottom: 1em;
      padding: 1em;
      background-color: #f6f6f6;
      border: 1px solid #999;
      border-radius: 3px;
      height: 100px;
      overflow: auto;
  }
.chosen-container{
  width: 100% !important;
}
.chosen{
  width: 100% !important;
}
</style>

@endsection
