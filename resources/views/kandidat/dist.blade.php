@extends('layouts.adminlte')

@section('content')

<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">{{$title}}</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn-sm  btn btn-success" name="button" onclick="newentry();"><i class="fa fa-plus-circle"></i>Tambah</button>
        </div>
      </div>
      <!-- /.card-header -->
      <div class="card-body table-responsive">


        Pilih Lembaga : 
        <div class="row">
          <div class="col-sm-4">
            <select class="form-control mb-2" name="pilem" id="pilem" onselect="GetTable()">
              <option value="">-Pilih-</option>
              @foreach($user as $dt)
              <option value="{{$dt->id}}">{{$dt->name}}</option>
              @endforeach
            </select>
          </div>
          <div class="col-sm-6">
            <i id="progress" name="progress"></i>
            <input type="hidden" name="pil_user" id="pil_user" value="0">
            <input type="hidden" name="pil_name" id="pil_name" value="--">
          </div>
        </div>


        <table class="table table-striped">
          <thead>
            <tr class="bg-warning">
              <th rowspan="2">Periode</th>
              <th colspan="4">Perolehan</th>
              <th colspan="3">Pendistribusian</th>
              <th rowspan="2" colspan="1">Saldo</th>
              <th rowspan="2" colspan="2">Action</th>
            </tr>
            <tr class="bg-warning">
              <th>Zakat</th>
              <th>Infaq</th>
              <th>Dana Sosial Keagamaan Lainnya</th>
              <th>Total</th>
              <th>Konsumtif</th>
              <th>Produktif</th>
              <th>Total</th>
            </tr>
          </thead>  
          <tbody id="isitb">
            
          </tbody>
        </table>
      </div>
      <!-- /.card-body -->
    </div>
    <!-- /.card -->

  </div>
  <!-- /.col -->
</div>
<!-- /.row -->



<!-- Modal -->
<div class="modal fade" id="modalcrud" tabindex="-1" aria-labelledby="modalcrudLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content ">
      <div class="modal-header">
        <h5 class="modal-title" id="modalcrudLabel">Entry Data</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <input type="hidden" name="id" id="id" value="0">
          <div class="col-md-8">
            <label for="">Nama Lembaga :</label>
            <input type="text" name="nama_lembaga" id="nama_lembaga" class="form-control" readonly/>
          </div>
          
          <div class="col-md-4 ml-auto">
            <label for="startDate">Periode :</label>
              <div class="datepicker-wrap">
                <div class="input-group">
                  <input type="text" class="form-control periode" placeholder="MM/YYYY" id="datepicker-input">
                  <span class="input-group-btn">
                      <button class="btn btn-default" type="button" id="datepicker-btn"><i class="fa fa-calendar" aria-hidden="true"></i></button>
                  </span>
                </div>
              </div>
          </div>

          <div class="col-md-12">
            <hr>
            <b>Perolehan : </b>
            <hr>
          </div>

          <div class="col-md-4 ">
            <label for="">Zakat</label>
            <input class="form-control" name="zakat" id="zakat" style="text-align:right" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" onblur="curr(this.name)">
          </div>
          
          <div class="col-md-4 ml-auto">
            <label for="">Infaq</label>
            <input class="form-control" name="infaq" id="infaq" style="text-align:right" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" onblur="curr(this.name)">
          </div>

          <div class="col-md-4 ml-auto">
            <label for="">Lainnya</label>
            <input class="form-control" name="lainnya" id="lainnya" style="text-align:right" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" onblur="curr(this.name)">
          </div>

          <div class="col-md-12">
            <hr>
            <b>Pendistribusian : </b>
            <hr>
          </div>

          <div class="col-md-6">
            <label for="">Konsumtif</label>
            <input class="form-control" name="konsumtif" id="konsumtif" style="text-align:right" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" onblur="curr(this.name)">
          </div>
          <div class="col-md-6">
            <label for="">Produktif</label>
            <input class="form-control" name="produktif" id="produktif" style="text-align:right" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" onblur="curr(this.name)">
          </div>
        </div>
        
      </div>
      <div class="modal-footer">
         <input type="hidden" name="act" id="act" value="new">
        <input onclick="saveData()" type="button" class="btn btn-success" value="Simpan Data">
      </div>
    </div>
  </div>
</div>


@endsection


@section('js')
<script type="text/javascript">
  
  function curr(name){
    var val = parseInt($('#' + name).val().replace(/[^0-9\.-]+/g,""));
    if(isNaN(val)){
      val = 0;
    }
    while (/(\d+)(\d{3})/.test(val.toString())){
      val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
    }
    $('#'+name).val(val);
  }

  function tocurr(val){
      while (/(\d+)(\d{3})/.test(val.toString())){
        val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
      }
      return val;
  }


  function clearItem(){
    $('#periode').val('');
    $('#zakat').val('');
    $('#infaq').val('');
    $('#lainnya').val('');
    $('#konsumtif').val('');
    $('#produktif').val('');
    $('#act').val('new');
  }



  function saveData(){
    var act = $('#act').val();
    var user_id = $('#pil_user').val();

    var id = $('#id').val();
    var periode = $('.periode').val();
    var zakat = parseInt($('#zakat').val().replace(/[^0-9\.-]+/g,""));
    var infaq = parseInt($('#infaq').val().replace(/[^0-9\.-]+/g,""));
    var dansos_lain = parseInt($('#lainnya').val().replace(/[^0-9\.-]+/g,""));
    var total_peroleh = zakat + infaq + dansos_lain;

    var konsumtif = parseInt($('#konsumtif').val().replace(/[^0-9\.-]+/g,""));
    var produktif = parseInt($('#produktif').val().replace(/[^0-9\.-]+/g,""));
    var total_distribusi = konsumtif + produktif ;

    var saldo = parseInt(total_peroleh) - parseInt(total_distribusi);
    if(periode==''){
      alert('Periode tidak boleh kosong');
      exit;
    }
    if(act=='new'){
      act = 'insert';
    }else{
      act = 'update';
    }
    $.ajax({
      "url":"{{url('admin/api/getdist')}}",
      "data": {act:act,id:id,user_id:user_id,periode:periode,zakat:zakat,infaq:infaq,dansos_lain:dansos_lain,total_peroleh:total_peroleh,konsumtif:konsumtif,produktif:produktif,total_distribusi:total_distribusi,saldo:saldo, "_token": "{{ csrf_token() }}"},
      "type":"POST",
      "dataType" :"json",
      "success":function(result){
          var hasil = JSON.parse(JSON.stringify(result));
          var cek = hasil.status;
          if(cek == 'Success'){
            loaddata(user_id);
            $('#modalcrud').modal('hide');
            clearItem();
          }else{
            alert(hasil.msg);
          }
      },
      "error":function(jqXHR, textStatus, errorThrown){
        alert(jqXHR.responseText);
      }
    });
  }



  function showedit(id){
    $("#progress").html('<img src="http://webzakat.esy.es/assets/images/ajax-loading30.gif">');
    $.ajax({
      "url":"{{url('admin/api/shownrc')}}/"+ id,
      "type": "GET",
      "dataType" : "json",
      "success":function(result){
         var hasil = JSON.parse(JSON.stringify(result));
         var res = JSON.parse(JSON.stringify(hasil.data));
          $('#act').val(hasil.data[0].id);
          $('#id').val(hasil.data[0].id);
          $('.periode').val(hasil.data[0].periode);
          $('#zakat').val(tocurr(hasil.data[0].zakat));
          $('#infaq').val(tocurr(hasil.data[0].infaq));
          $('#lainnya').val(tocurr(hasil.data[0].dansos_lain));
          $('#konsumtif').val(tocurr(hasil.data[0].konsumtif));
          $('#produktif').val(tocurr(hasil.data[0].produktif));
          $('#modalcrud').modal('show');
          $("#progress").html('');
      }
    });
  }



  function showdel(id) {
    var r = confirm("Are you sure you want to delete this Record?");
    if (r == true) {
        $("#progress").html('<img src="http://webzakat.esy.es/assets/images/ajax-loading30.gif">');
        $.ajax({
          "url":"{{url('admin/api/deletenrc')}}/"+ id,
          "type": "GET",
          "dataType" : "json",
          "success":function(result){
            var hasil = JSON.parse(JSON.stringify(result));
            var cek = hasil.status;
            if(cek == 'Success'){
              loaddata($('#pil_user').val());
              $("#progress").html('');
            }else{
              alert(hasil.msg);
            }
          }
        });
    }
  }

  


  loaddata($('#pil_user').val());
  function loaddata(id) {
    $("#progress").html('<img src="http://webzakat.esy.es/assets/images/ajax-loading30.gif">');
    $('#callbtn').html('<input type="submit" class="btn btn-info" value="Save" disabled>');
    $.ajax({
      "url": "{{url('admin/lpz/tbgetdist')}}/"+id,
      "type":"GET",
      "success":function(data){
        document.getElementById('isitb').innerHTML = data;
        $("#progress").html('');
        if(id==0){
          $('#callbtn').html('<input type="submit" class="btn btn-info" value="Save" disabled>');
        }else {
          $('#callbtn').html('<input type="submit" class="btn btn-info" value="Save">');
        }
      }
    });
  }



$(function(){
  var selCategory = $('#pilem');
  $(selCategory).change(function() {
    $('option:selected').each(function() {
      loaddata($(this).val());
      $('#pil_user').val($(this).val());
      $('#pil_name').val($(this).text());
      $('#nama_lembaga').val($(this).text());
    });
  });
});

function newentry() {
  if($('#pil_user').val()!=0){
    $('#modalcrud').modal('show');
  }else {
    alert('Nama lembaga tidak boleh kosong');
  } 
}

</script>
@endsection