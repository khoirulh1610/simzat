<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/', 'LandingController@index')->name('index');
Route::post('/cari', 'LandingController@pencarian')->name('cari');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/pengaduan/store', 'LandingController@PengaduanStore')->name('aduan.store');

Route::group(['prefix'=>'admin','as'=>'admin.'],function (){
	Route::get('/home','HomeController@index')->name('home');

	Route::get('/kandidat/profil','KandidatController@profil')->name('profil');
	Route::get('/kandidat/document','KandidatController@document')->name('document');
	Route::get('/lpz/tbsyarat/{slug}','KandidatController@syarat')->name('syarat');
	Route::POST('/lpz/url_pdf/{slug}','KandidatController@url_pdf')->name('url_pdf');
	Route::POST('/lpz/upload_file','KandidatController@upload_file')->name('upload_file');
	Route::POST('/lpz/del_pdf','KandidatController@del_pdf')->name('del_pdf');

	Route::get('/kandidat/getdist','KandidatController@getdist')->name('getdist');
	Route::get('/lpz/tbgetdist/{slug}','KandidatController@tbgetdist')->name('tbgetdist');
	Route::post('/api/getdist','KandidatController@getdistapi')->name('getdistapi');
	Route::get('/api/deletenrc/{slug}','KandidatController@deletenrc')->name('deletenrc');
	Route::get('/api/shownrc/{slug}','KandidatController@shownrc')->name('shownrc');

	Route::get('/audit/import','AuditController@import')->name('import');
	Route::get('/audit/kinerja','AuditController@kinerja')->name('kinerja');
	Route::get('/audit/kinerja_nilai/{slug}','AuditController@kinerja_nilai')->name('kinerja_nilai');
	Route::post('/audit/postkinerja','AuditController@postkinerja')->name('postkinerja');
	Route::post('/audit/nilaikinerja','AuditController@nilaikinerja')->name('nilaikinerja');
	Route::get('/audit/kepatuhan','AuditController@kepatuhan')->name('kepatuhan');
	Route::get('/audit/kepatuhan_nilai/{slug}','AuditController@kepatuhan_nilai')->name('kepatuhan_nilai');
	Route::post('/audit/postnilai','AuditController@postnilai')->name('postnilai');
	Route::post('/audit/getsubnilai','AuditController@getsubnilai')->name('getsubnilai');

	Route::get('/lembaga/verifikasi','LembagaController@verifikasi')->name('verifikasi');
	Route::post('/lembaga/verifikasi/update','LembagaController@update')->name('editverifikasi');

	Route::GET('/aduan', 'PengaduanController@index')->name('aduan');

	Route::GET('/user', 'UserController@index')->name('user');
	Route::GET('/loaduser', 'UserController@loaduser')->name('loaduser');
	Route::POST('/saveuser', 'UserController@store')->name('saveuser');
	Route::GET('/deleteuser/{slug}', 'UserController@delete')->name('deleteuser');
	Route::GET('/showuser/{slug}', 'UserController@show')->name('showuser');

	Route::GET('/showgroup/{id}','UserController@showgroup');
	Route::POST('/tambah-akses','UserController@tambahAkses');
	Route::GET('/akses-data/{id}','UserController@AksesData');
	Route::GET('/akses-hapus/{id}','UserController@AksesHapus');

	Route::get('/question','HomeController@index')->name('question');

});