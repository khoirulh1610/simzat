/*
 Navicat Premium Data Transfer

 Source Server         : 192.168.0.60_3306
 Source Server Type    : MariaDB
 Source Server Version : 100322
 Source Host           : 192.168.0.60:3306
 Source Schema         : zakat

 Target Server Type    : MariaDB
 Target Server Version : 100322
 File Encoding         : 65001

 Date: 02/09/2020 06:44:49
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for lpz_kepatuhan
-- ----------------------------
DROP TABLE IF EXISTS `lpz_kepatuhan`;
CREATE TABLE `lpz_kepatuhan`  (
  `user_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `patuh_subno` char(6) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `patuh_val` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `patuh_skor` int(11) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of lpz_kepatuhan
-- ----------------------------
INSERT INTO `lpz_kepatuhan` VALUES ('16', '1.1', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('16', '1.2', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('16', '1.3', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('16', '1.4', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('16', '1.5.a', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('16', '1.5.b', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('16', '1.5.c', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('16', '1.5.d', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('16', '1.5.e', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('16', '1.6', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('16', '1.7', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('16', '1.8', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('16', '1.9', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('16', '1.10', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('16', '1.11', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('16', '2.1', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('16', '2.2', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('16', '2.3', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('16', '2.4', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('16', '2.5', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('16', '2.6', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('16', '2.7', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('16', '2.8', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('16', '2.9', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('16', '2.10', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('16', '3.1', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('16', '3.2', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('16', '3.3', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('16', '3.4', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('16', '3.5', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('16', '3.6', '0', 0);
INSERT INTO `lpz_kepatuhan` VALUES ('16', '3.7', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('16', '3.8', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('16', '3.9', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('7', '1.1', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('7', '1.2', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('17', '1.1', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('17', '1.2', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('17', '1.3', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('17', '1.4', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('17', '1.5.a', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('17', '1.5.c', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('17', '1.5.e', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('17', '1.5.d', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('17', '1.6', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('17', '1.7', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('17', '1.8', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('7', '1.3', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('7', '2.1', '0', 0);
INSERT INTO `lpz_kepatuhan` VALUES ('15', '1.1', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('15', '1.2', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('7', '1.4', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('7', '1.5.a', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('7', '1.5.b', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('7', '1.5.c', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('7', '1.5.d', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('7', '1.5.e', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('7', '1.8', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('7', '1.11', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('7', '1.10', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('7', '1.9', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('7', '1.7', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('7', '1.6', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('7', '3.9', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('7', '2.9', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('7', '2.10', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('18', '3.8', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('18', '3.9', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('7', '3.8', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('7', '3.7', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('46', '1.11', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('46', '2.1', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('46', '1.10', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('46', '3.9', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('46', '3.8', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('46', '3.7', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('46', '3.6', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('46', '3.5', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('46', '3.4', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('46', '3.3', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('46', '3.2', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('46', '3.1', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('46', '2.10', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('46', '2.9', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('46', '2.8', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('46', '2.6', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('46', '2.5', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('46', '2.3', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('46', '1.4', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('46', '1.2', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('46', '1.1', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('7', '3.6', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('15', '1.9', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('15', '1.5.a', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('15', '1.5.c', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('15', '3.6', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('15', '2.1', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('18', '3.7', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('18', '3.6', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('18', '3.5', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('18', '1.1', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('18', '1.10', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('18', '1.11', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('18', '1.2', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('18', '1.3', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('18', '1.4', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('18', '1.5.a', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('18', '1.5.b', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('18', '1.5.c', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('18', '1.5.e', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('18', '1.5.d', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('18', '1.6', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('18', '1.7', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('18', '1.8', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('18', '1.9', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('18', '2.1', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('18', '2.10', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('18', '2.2', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('18', '2.3', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('18', '2.4', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('18', '2.5', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('18', '2.6', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('18', '2.7', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('18', '2.8', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('18', '2.9', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('18', '3.1', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('18', '3.2', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('18', '3.3', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('18', '3.4', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('7', '2.2', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('7', '2.3', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('7', '2.4', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('7', '2.5', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('7', '2.6', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('7', '2.7', '0', 0);
INSERT INTO `lpz_kepatuhan` VALUES ('7', '2.8', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('7', '3.1', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('7', '3.2', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('7', '3.3', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('7', '3.4', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('7', '3.5', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('19', '1.1', '0', 0);
INSERT INTO `lpz_kepatuhan` VALUES ('20', '1.1', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('20', '1.10', '2', 10);
INSERT INTO `lpz_kepatuhan` VALUES ('20', '1.11', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('20', '1.2', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('20', '1.3', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('20', '1.4', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('20', '1.5.a', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('20', '1.5.b', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('20', '1.5.c', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('20', '1.5.d', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('20', '1.6', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('20', '1.5.e', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('20', '1.7', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('20', '1.8', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('20', '1.9', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('20', '2.1', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('20', '2.10', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('20', '2.2', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('20', '2.3', '0', 0);
INSERT INTO `lpz_kepatuhan` VALUES ('20', '2.4', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('20', '2.5', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('20', '2.6', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('20', '2.7', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('20', '2.8', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('20', '2.9', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('20', '3.1', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('20', '3.3', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('20', '3.2', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('20', '3.5', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('20', '3.4', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('20', '3.6', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('20', '3.8', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('20', '3.9', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('20', '3.7', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('21', '1.1', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('21', '1.10', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('21', '1.11', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('21', '1.2', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('21', '1.3', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('21', '1.4', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('21', '1.5.a', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('21', '1.5.b', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('21', '1.5.d', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('21', '1.5.c', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('21', '1.5.e', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('21', '1.6', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('21', '1.7', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('21', '1.9', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('21', '2.1', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('21', '2.10', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('21', '2.2', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('21', '2.3', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('21', '2.5', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('21', '2.4', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('21', '2.6', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('21', '2.7', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('21', '2.8', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('21', '2.9', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('21', '3.1', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('21', '3.2', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('21', '3.3', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('21', '3.4', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('21', '3.5', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('21', '3.6', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('21', '3.7', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('21', '3.8', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('21', '3.9', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('', '1.1', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('', '1.10', '1', 5);
INSERT INTO `lpz_kepatuhan` VALUES ('6', '1.1', '0', 0);

-- ----------------------------
-- Table structure for lpz_kinerja
-- ----------------------------
DROP TABLE IF EXISTS `lpz_kinerja`;
CREATE TABLE `lpz_kinerja`  (
  `user_id` char(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `kin_no` char(5) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `kin_subno` char(6) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `kin_val` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `kin_skor` int(11) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of lpz_kinerja
-- ----------------------------
INSERT INTO `lpz_kinerja` VALUES ('15', 'E.4', '4.1', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'E.4', '4.2', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'E.4', '4.3', 'C', 2);
INSERT INTO `lpz_kinerja` VALUES ('15', 'E.5', '5.1', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'E.5', '5.2', 'C', 2);
INSERT INTO `lpz_kinerja` VALUES ('15', 'E.3', '3.2', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'E.3', '3.1', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'E.2', '2.3', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'E.2', '2.2', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'E.2', '2.1', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'E.1', '1.4', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'E.1', '1.3', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'E.1', '1.2', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'E.1', '1.1', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'D.7', '7.2', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'D.7', '7.1', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'D.6', '6.3', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'D.6', '6.2', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'D.6', '6.1', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'D.5', '5.2', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'D.5', '5.1', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'D.4', '4.4', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'D.4', '4.3', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'D.4', '4.2', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'D.4', '4.1', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'D.3', '3.2', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'D.3', '3.1', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'D.2', '2.3', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'D.2', '2.2', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'D.2', '2.1', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'D.1', '1.4', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'D.1', '1.3', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'D.1', '1.2', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'D.1', '1.1', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'C.3', '3.12', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'C.3', '3.11', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'C.3', '3.10', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'C.3', '3.9', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'C.3', '3.8', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'C.3', '3.7', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'C.3', '3.5', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'C.3', '3.6', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'C.3', '3.4', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'C.3', '3.3', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'C.3', '3.2', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'C.3', '3.1', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'C.2', '2.3', 'B', 3);
INSERT INTO `lpz_kinerja` VALUES ('15', 'C.2', '2.2', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'C.2', '2.1', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'C.1', '1.5', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'C.1', '1.4', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'C.1', '1.3', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'C.1', '1.2', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'C.1', '1.1', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'B.4', '4.24', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'B.4', '4.23', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'B.4', '4.22', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'B.4', '4.21', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'B.4', '4.20', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'B.4', '4.19', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'B.4', '4.18', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'B.4', '4.17', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'B.4', '4.16', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'B.4', '4.15', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'B.4', '4.14', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'B.4', '4.13', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'B.4', '4.12', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'B.4', '4.11', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'B.4', '4.10', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'B.4', '4.9', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'B.4', '4.8', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'B.4', '4.7', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'B.4', '4.6', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'B.4', '4.5', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'B.4', '4.4', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'B.4', '4.3', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'B.4', '4.2', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'B.4', '4.1', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'B.3', '3.2', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'B.3', '3.1', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'B.2', '2.2', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'B.2', '2.1', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'B.1', '1.3', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'B.1', '1.2', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'B.1', '1.1', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'A.6', '6.4', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'A.6', '6.3', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'A.6', '6.2', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'A.6', '6.1', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'A.5', '5.7', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'A.5', '5.6', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'A.5', '5.5', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'A.5', '5.4', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'A.5', '5.3', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'A.5', '5.2', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'A.5', '5.1', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'A.4', '4.4', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'A.4', '4.3', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'A.4', '4.2', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'A.4', '4.1', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'A.3', '3.4', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'A.3', '3.3', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'A.3', '3.2', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'A.3', '3.1', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'A.2', '2.5', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'A.2', '2.4', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'A.2', '2.3', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'A.2', '2.2', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'A.2', '2.1', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'A.1', '1.2', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'A.1', '1.3', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('15', 'A.1', '1.1', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('6', 'A.1', '1.1', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('6', 'A.1', '1.2', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('6', 'A.1', '1.3', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('6', '', '', 'A', 4);
INSERT INTO `lpz_kinerja` VALUES ('38', '', '', 'A', 4);

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of migrations
-- ----------------------------

-- ----------------------------
-- Table structure for mst_kepatuhan
-- ----------------------------
DROP TABLE IF EXISTS `mst_kepatuhan`;
CREATE TABLE `mst_kepatuhan`  (
  `patuh_no` char(2) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `patuh_subno` char(6) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `patuh_variabel` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mst_kepatuhan
-- ----------------------------
INSERT INTO `mst_kepatuhan` VALUES ('1', '1.0', 'Pengujian substansi terhadap pengumpulan Zakat');
INSERT INTO `mst_kepatuhan` VALUES ('1', '1.1', 'Memastikan apakah harta muzakki diperoleh dengan cara halal');
INSERT INTO `mst_kepatuhan` VALUES ('1', '1.2', 'Memastikan apakah Muzakki memperoleh Bukti setoran Zakat yang sah disertai dengan ijab kabul.');
INSERT INTO `mst_kepatuhan` VALUES ('1', '1.3', 'Memastikan apakah tanggal haul, yaitu tanggal mulainya dihitung zakat dari muzaki sesuai dengan syarat wajib zakat.');
INSERT INTO `mst_kepatuhan` VALUES ('1', '1.4', 'Memeriksa apakah dalam menghitung harta kekayaan keseluruhan wajib zakat telah dilakukan secara benar  sesuai prinsip-prinsip syariah sehingga dapat diyakinkan bahwa harta tersebut benar benar milik muzakki dan merupakan penggabungan dari pendapatan muzak');
INSERT INTO `mst_kepatuhan` VALUES ('1', '1.5', 'Memastikan bahwa zakat yang akan dikenakan sesuai dengan prinsip-prinsip syariah (PMA No. 52 Tahun 2014)');
INSERT INTO `mst_kepatuhan` VALUES ('1', '1.5.a', '2,5 % untuk harta perdagangan');
INSERT INTO `mst_kepatuhan` VALUES ('1', '1.5.b', '5% hasil pertanian tanpa biaya pengairan');
INSERT INTO `mst_kepatuhan` VALUES ('1', '1.5.c', '10 % hasil pertanian dengan biaya pengairan');
INSERT INTO `mst_kepatuhan` VALUES ('1', '1.5.d', '20 % barang galian tambang');
INSERT INTO `mst_kepatuhan` VALUES ('1', '1.5.e', 'Peternakan sesuai dengan PMA No. 52 tahun 2014');
INSERT INTO `mst_kepatuhan` VALUES ('1', '1.6', 'Memastikan bahwa amil dalam melakukan pembukuan dana ZIS memisahkan antara dana zakat dan dana infak serta dana sosial keagamaan lainnya.');
INSERT INTO `mst_kepatuhan` VALUES ('1', '1.7', 'Memastikan bahwa Hak Amil diambil maksimal 12,5 %  dari penggabungan zakat yang diperolah dari tingkat pusat hingga cabang/ unit dibawahnya');
INSERT INTO `mst_kepatuhan` VALUES ('1', '1.8', 'Memastikan bahwa dana zakat yang terkumpul sesuai dengan wilayah operasional Lembaga Pengelola Zakat yang bersangkutan.');
INSERT INTO `mst_kepatuhan` VALUES ('1', '1.9', 'Memastikan bahwa harta yang dikenakan zakat telah mencapai nishab sebesar 85 gr. emas untuk harta dan ketentuan lain sesuai dengan PMA No. 52 Tahun 2014');
INSERT INTO `mst_kepatuhan` VALUES ('1', '1.10', 'Memastikan bahwa amil dalam mengajak kesadaran muzaki untuk berzakat dan menerima zakat dari muzaki (amil pengumpulan) tidak menjanjikan, memberikan atau menerima sesuatu hadiah dan fasilitas apapun yang diambil dari dana zakat, infak sedekah dan dana sos');
INSERT INTO `mst_kepatuhan` VALUES ('1', '1.11', 'Memastikan bahwa zakat perusahaan apabila menerima telah dihitung dengan benar');
INSERT INTO `mst_kepatuhan` VALUES ('2', '2.0', 'Pengujian substansi terhadap pendistribusian dan pendayagunaan zakat');
INSERT INTO `mst_kepatuhan` VALUES ('2', '2.1', 'Memastikan zakat yang dikelola oleh lembaga  pengelola zakat disalurkan kepada mustahik sesuai asnaf yang ada/ 8 asnaf.');
INSERT INTO `mst_kepatuhan` VALUES ('2', '2.2', 'Memeriksa apakah penyaluran zakat dilakukan dalam tahun pembukuan yang berjalan');
INSERT INTO `mst_kepatuhan` VALUES ('2', '2.3', 'Memeriksa dan memastikan penyaluran zakat bebas dari kepentingan lain di luar kepentingan mustahik.');
INSERT INTO `mst_kepatuhan` VALUES ('2', '2.4', 'Memastikan bahwa zakat yang telah diterima oleh mustahik tidak ada kewajiban mengembalikan kepada lembaga  pengelola zakat.');
INSERT INTO `mst_kepatuhan` VALUES ('2', '2.5', 'Memastikan lembaga  pengelola zakat memiliki standar waktu penyelesaian penyaluran sehingga mustahik menerima haknya dalam waktu yang cepat dan tidak terhambat birokrasi yang lama.');
INSERT INTO `mst_kepatuhan` VALUES ('2', '2.6', 'Memeriksa dan memastikan bahwa amil pendistribusian tidak menerima hadiah atau sesuatu pemberian dari mustahik berkaitan dengan tugasnya sebagai amil. ');
INSERT INTO `mst_kepatuhan` VALUES ('2', '2.7', 'Memeriksa dan memastikan seluruh zakat yang diterima di tahun berjalan disalurkan seluruhnya atau minimal 80 % dari total penerimaan di tahun tersebut.');
INSERT INTO `mst_kepatuhan` VALUES ('2', '2.8', 'Memastikan bahwa zakat yang didistribusikan tidak digunakan demi kepentingan lembaga lain yang tidak memiliki manfaat langsung bagi mustahik.');
INSERT INTO `mst_kepatuhan` VALUES ('2', '2.9', 'Memastikan bahwa zakat yang didayagunakan tidak dipergunakan untuk mencari keuntungan/diinvestasikan untuk kepentingan bisnis yang menguntungkan amil penyaluran.');
INSERT INTO `mst_kepatuhan` VALUES ('2', '2.10', 'Memastikan bahwa zakat tidak didepositokan/ disimpan sehingga mendapat bunga dan bentuk riba lainnya.');
INSERT INTO `mst_kepatuhan` VALUES ('3', '3.0', 'Pengujian substansi terhadap pengelolaan keuangan zakat');
INSERT INTO `mst_kepatuhan` VALUES ('3', '3.1', 'Memastikan seluruh dana (ZIS) yang dikumpulkan penempatannya di bank syariah.');
INSERT INTO `mst_kepatuhan` VALUES ('3', '3.2', 'Memastikan dana operasional amil yang berasal dari dana zakat tidak boleh melebihi 12,5 % dari total perolehan zakat di tahun berjalan.');
INSERT INTO `mst_kepatuhan` VALUES ('3', '3.3', 'Memastikan pendapatan dan gaji amil wajar dan transparan sesuai kepantasan dalam menjalankan tugas sebagai amil');
INSERT INTO `mst_kepatuhan` VALUES ('3', '3.4', 'Memastikan aset lembaga pengelola zakat tidak beralih fungsi dan kepemilikan menjadi milik perseorangan/pihak lain.');
INSERT INTO `mst_kepatuhan` VALUES ('3', '3.5', 'Memastikan bahwa amil pengelolaan zakat tidak menerima hadiah dari pihak ketiga dalam kaitannya dengan pengelolaan dana zakat dan pengadaan barang dan jasa.');
INSERT INTO `mst_kepatuhan` VALUES ('3', '3.6', 'Memastikan sisa hak amil jika masih ada, dimaanfaatkan untuk kepentingan lembaga.');
INSERT INTO `mst_kepatuhan` VALUES ('3', '3.7', 'Memastikan amil tidak melakukan kegiatan bisnis terkait dengan kelembagaan dikarenakan wewenang yang melekat kepada amil dalam hal pengelolaan dana zakat.');
INSERT INTO `mst_kepatuhan` VALUES ('3', '3.8', 'Memastikan bahwa rekruitmen amil bersifat transparan dan menggunakan sistem yang bebas KKN.');
INSERT INTO `mst_kepatuhan` VALUES ('3', '3.9', 'Memastikan sistem IT  dan software yang digunakan dalam pengelolaan dana zakat mendukung pengelolaan zakat secara syariah (misalnya SIMBA, dsb)');

-- ----------------------------
-- Table structure for mst_kinerja
-- ----------------------------
DROP TABLE IF EXISTS `mst_kinerja`;
CREATE TABLE `mst_kinerja`  (
  `kin_no` char(4) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `kin_subno` char(6) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `kin_variabel` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `kin_bobot` int(11) NULL DEFAULT 0
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mst_kinerja
-- ----------------------------
INSERT INTO `mst_kinerja` VALUES ('A.0', NULL, 'Kinerja Manajemen Kelembagaan (dalam 3 tahun terakhir)', 0);
INSERT INTO `mst_kinerja` VALUES ('A.1', '1.0', 'Rencana Strategis', 0);
INSERT INTO `mst_kinerja` VALUES ('A.1', '1.1', 'Visi dan Misi Lembaga tercantum dengan jelas dan dapat dipahami oleh semua amil lembaga pengelola zakat', 1);
INSERT INTO `mst_kinerja` VALUES ('A.1', '1.2', 'Lembaga memiliki tujuan, target dan sasaran dan dipublikasikan dalam renstra', 1);
INSERT INTO `mst_kinerja` VALUES ('A.1', '1.3', 'Lembaga memiliki program kerja dalam 3 tahun terakhir dan telah dipublikasikan dalam renstra', 1);
INSERT INTO `mst_kinerja` VALUES ('A.2', '2.0', 'Program Kerja Pengelolaan Zakat', 0);
INSERT INTO `mst_kinerja` VALUES ('A.2', '2.1', 'Lembaga memiliki program kerja dibidang pengumpulan', 1);
INSERT INTO `mst_kinerja` VALUES ('A.2', '2.2', 'Lembaga memiliki program kerja dibidang pendistribusian', 1);
INSERT INTO `mst_kinerja` VALUES ('A.2', '2.3', 'Lembaga memiliki program kerja dibidang pendayagunaan', 1);
INSERT INTO `mst_kinerja` VALUES ('A.2', '2.4', 'Lembaga memiliki program pengembangan zakat ', 1);
INSERT INTO `mst_kinerja` VALUES ('A.2', '2.5', 'Lembaga melaksanakan rapat koordinasi dan rapat kerja zakat', 1);
INSERT INTO `mst_kinerja` VALUES ('A.3', '3.0', 'SOP pengelolaan zakat (pengumpulan, pendistribusian dan pendayagunaan)', 0);
INSERT INTO `mst_kinerja` VALUES ('A.3', '3.1', 'Lembaga memiliki SOP dibidang pengumpulan zakat', 1);
INSERT INTO `mst_kinerja` VALUES ('A.3', '3.2', 'Lembaga memiliki SOP di bidang pendistribusian zakat', 1);
INSERT INTO `mst_kinerja` VALUES ('A.3', '3.3', 'Lembaga memiliki SOP dibidang pendayagunaan zakat', 1);
INSERT INTO `mst_kinerja` VALUES ('A.3', '3.4', 'Lembaga memiliki SOP dibidang lainnya selain pengumpulan, pendistribusian dan pendayagunaan zakat', 1);
INSERT INTO `mst_kinerja` VALUES ('A.4', '4.0', 'Regulasi zakat pusat/daerah (PP,Perda, Instruksi Gubernur, dll)', 0);
INSERT INTO `mst_kinerja` VALUES ('A.4', '4.1', 'Pemerintah Pusat/Daerah setempat telah mengeluarkan PP/Perda dibidang zakat dalam mendukung optimalisasi zakat ', 1);
INSERT INTO `mst_kinerja` VALUES ('A.4', '4.2', 'Pemerintah Pusat/Daerah setempat telah mengeluarkan instruksi/surat edaran dalam mendukung optimalisasi zakat', 1);
INSERT INTO `mst_kinerja` VALUES ('A.4', '4.3', 'Lembaga memiliki fatwa dalam pengelolaan zakat agar sesuai dengan prinsip syariah', 1);
INSERT INTO `mst_kinerja` VALUES ('A.4', '4.4', 'Lembaga memiliki regulasi lainnya dalam upaya optimalisasi zakat', 1);
INSERT INTO `mst_kinerja` VALUES ('A.5', '5.0', 'Laporan Keuangan lembaga sesuai dengan standar keuangan yang berlaku umum (PSAK 109)', 0);
INSERT INTO `mst_kinerja` VALUES ('A.5', '5.1', 'Laporan keuangan memisahkan pencatatan antara dana zakat dan dana infak sedekah', 2);
INSERT INTO `mst_kinerja` VALUES ('A.5', '5.2', 'Laporan Keuangan mencantumkan penggunaan hak amil/ dana operasional lembaga secara efektif dan efisien', 2);
INSERT INTO `mst_kinerja` VALUES ('A.5', '5.3', 'Laporan Keuangan mencantumkan perolehan aset lembaga secara lengkap', 2);
INSERT INTO `mst_kinerja` VALUES ('A.5', '5.4', 'Laporan Keuangan telah disusun sesuai dengan PSAK No. 109', 2);
INSERT INTO `mst_kinerja` VALUES ('A.5', '5.5', 'Laporan Keuangan telah diaudit oleh Kantor Akuntan Publik', 2);
INSERT INTO `mst_kinerja` VALUES ('A.5', '5.6', 'Laporan Keuangan telah menggambarkan kenaikan kinerja dari tahun sebelumnya', 2);
INSERT INTO `mst_kinerja` VALUES ('A.5', '5.7', 'Laporan Keuangan telah dipublikasikan kepada publik melalui media elektronik dan media cetak', 2);
INSERT INTO `mst_kinerja` VALUES ('A.6', '6.0', 'Struktur legalitas Lembaga Pengelola Zakat sesuai UU Nomor 23 Tahun 2011 dan PP Nomor 14 Tahun 2014', 0);
INSERT INTO `mst_kinerja` VALUES ('A.6', '6.1', 'Lembaga telah memiliki surat izin operasional resmi yang dikeluarkan oleh pemerintah', 1);
INSERT INTO `mst_kinerja` VALUES ('A.6', '6.2', 'Lembaga telah memiliki kepengurusan resmi sesuai dengan UU No. 23 Tahun 2011 dan PP Nomor 14 tahun 2014', 1);
INSERT INTO `mst_kinerja` VALUES ('A.6', '6.3', 'Lembaga telah menyeleksi kepengurusan secara transparan dan terbuka kepada publik', 1);
INSERT INTO `mst_kinerja` VALUES ('A.6', '6.4', 'Lembaga telah memiliki pelaksana harian yang bertugas rutin dengan jadwal yang teratur', 1);
INSERT INTO `mst_kinerja` VALUES ('B.0', NULL, 'Kinerja Keamilan', 0);
INSERT INTO `mst_kinerja` VALUES ('B.1', '1.0', 'Data kepegawaian amil Lembaga Pengelola Zakat', 0);
INSERT INTO `mst_kinerja` VALUES ('B.1', '1.1', 'Visi dan Misi Lembaga tercantum dengan jelas dan dapat dipahami oleh semua amil lembaga pengelola zakat', 1);
INSERT INTO `mst_kinerja` VALUES ('B.1', '1.2', 'Lembaga memiliki program pengembangan karir amil', 1);
INSERT INTO `mst_kinerja` VALUES ('B.1', '1.3', 'Lembaga melakukan pengarsipan data amil secara rapi', 1);
INSERT INTO `mst_kinerja` VALUES ('B.2', '2.0', 'Program pembinaan dan pelatihan amil secara simultan', 0);
INSERT INTO `mst_kinerja` VALUES ('B.2', '2.1', 'Lembaga memiliki jadwal program pembinaan untuk mengembangkan sumberdaya amil', 1);
INSERT INTO `mst_kinerja` VALUES ('B.2', '2.2', 'Lembaga melaksanakan program pelatihan zakat untuk meningkatkan kompetensi amil ', 1);
INSERT INTO `mst_kinerja` VALUES ('B.3', '3.0', 'Program Evaluasi Kinerja Keamilan', 0);
INSERT INTO `mst_kinerja` VALUES ('B.3', '3.1', 'Lembaga melakukan evaluasi kinerja amil dalam 1 tahun', 1);
INSERT INTO `mst_kinerja` VALUES ('B.3', '3.2', 'Lembaga memberi insentif apabila mencapai target lembaga atau gaji yang diberikan setiap bulan secara reguler', 1);
INSERT INTO `mst_kinerja` VALUES ('B.4', '4.0', 'Kompetensi Amil Zakat', 0);
INSERT INTO `mst_kinerja` VALUES ('B.4', '4.1', 'Amil Zakat menguasai jenis-jenis zakat dan cara menghitung zakat', 1);
INSERT INTO `mst_kinerja` VALUES ('B.4', '4.2', 'Amil zakat menguasai 8 asnaf mustahik dan batasan penentuannya', 1);
INSERT INTO `mst_kinerja` VALUES ('B.4', '4.3', 'Amil Zakat menguasai ukuran kemiskinan dan batasan penghasilan untuk menjadi muzakki/mustahik', 1);
INSERT INTO `mst_kinerja` VALUES ('B.4', '4.4', 'Amil zakat memiliki pemahan dan manajemen zakat yang memadai', 1);
INSERT INTO `mst_kinerja` VALUES ('B.4', '4.5', 'Amil zakat  memiliki pemahaman dan kecakapan dalam berorganisasi', 1);
INSERT INTO `mst_kinerja` VALUES ('B.4', '4.6', 'Amil zakat melakukan pelayanan rutin sesuai jam kerja kepada donatur dan mustahik', 1);
INSERT INTO `mst_kinerja` VALUES ('B.4', '4.7', 'Amil Zakat memberikan kemudahan dan tidak mempersulit muzaki/mustahik', 1);
INSERT INTO `mst_kinerja` VALUES ('B.4', '4.8', 'Amil Zakat melakukan 3 S (Sapa, Senyum, Sopan)', 1);
INSERT INTO `mst_kinerja` VALUES ('B.4', '4.9', 'Amil zakat berintegritas, jujur dan amanah dalam menjalankan tugas', 1);
INSERT INTO `mst_kinerja` VALUES ('B.4', '4.10', 'Amil zakat memiliki kecermatan dan ketelitian', 1);
INSERT INTO `mst_kinerja` VALUES ('B.4', '4.11', 'Amil zakat memberikan informasi yang diberikan sesuai dengan yang diketahuinya', 1);
INSERT INTO `mst_kinerja` VALUES ('B.4', '4.12', 'Amil zakat berani tidak malu untuk menyatakan tidak tahu jika yang ditanyakan tidak sesuai dengan kapasitasnya', 1);
INSERT INTO `mst_kinerja` VALUES ('B.4', '4.13', 'Amil zakat tidak mendesak donatur dalam memungut ZIS dengan sesuatu yang tidak disukai', 1);
INSERT INTO `mst_kinerja` VALUES ('B.4', '4.14', 'Amil zakat menghargai waktu dan kesibukan donatur', 1);
INSERT INTO `mst_kinerja` VALUES ('B.4', '4.15', 'Amil zakat menepati janji', 1);
INSERT INTO `mst_kinerja` VALUES ('B.4', '4.16', 'Amil zakat mengucapkan terimakasih', 1);
INSERT INTO `mst_kinerja` VALUES ('B.4', '4.17', 'Amil Zakat segera meminta maaf apabila melakukan kesalahan', 1);
INSERT INTO `mst_kinerja` VALUES ('B.4', '4.18', 'Amil zakat memiliki komitmen tinggi', 1);
INSERT INTO `mst_kinerja` VALUES ('B.4', '4.19', 'Amil zakat mencintai profesinya', 1);
INSERT INTO `mst_kinerja` VALUES ('B.4', '4.20', 'Amil zakat memiliki inovasi dan kreatifitas yang tinggi', 1);
INSERT INTO `mst_kinerja` VALUES ('B.4', '4.21', 'Amil zakat mampu menjalin kerja sama dengan lembaga lain', 1);
INSERT INTO `mst_kinerja` VALUES ('B.4', '4.22', 'Amil zakat mampu menjalin kerjasama tim', 1);
INSERT INTO `mst_kinerja` VALUES ('B.4', '4.23', 'Amil zakat berpakaian rapi sesuai syariat', 1);
INSERT INTO `mst_kinerja` VALUES ('B.4', '4.24', 'Amil zakat tidak merokok ', 1);
INSERT INTO `mst_kinerja` VALUES ('C.0', NULL, 'Sarana dan Prasarana', 0);
INSERT INTO `mst_kinerja` VALUES ('C.1', '1.0', 'Kelengkapan prasarana kantor ', 0);
INSERT INTO `mst_kinerja` VALUES ('C.1', '1.1', 'Visi dan Misi Lembaga tercantum dengan jelas dan dapat dipahami oleh semua amil lembaga pengelola zakat', 1);
INSERT INTO `mst_kinerja` VALUES ('C.1', '1.2', 'Lembaga memiliki nomor telepon dan faksimili', 1);
INSERT INTO `mst_kinerja` VALUES ('C.1', '1.3', 'Lembaga mengadministrasikan data aset dan lain-lain lembaga secara rapi', 1);
INSERT INTO `mst_kinerja` VALUES ('C.1', '1.4', 'Lembaga memiliki mobil operasional', 1);
INSERT INTO `mst_kinerja` VALUES ('C.1', '1.5', 'Lembaga memiliki layanan internet untuk perkantoran', 1);
INSERT INTO `mst_kinerja` VALUES ('C.2', '2.0', 'Kelayakan gedung', 0);
INSERT INTO `mst_kinerja` VALUES ('C.2', '2.1', 'Kantor Lembaga merupakan kantor yang tetap dan tidak berpindah lokasi dalam waktu yang lama', 1);
INSERT INTO `mst_kinerja` VALUES ('C.2', '2.2', 'Kantor lembaga berlokasi  di tempat yang cukup representatif dalam melakukan pelayanan zakat', 1);
INSERT INTO `mst_kinerja` VALUES ('C.2', '2.3', 'Gedung memiliki struktur bangunan yang kokoh dan kuat serta layak ditempati', 1);
INSERT INTO `mst_kinerja` VALUES ('C.3', '3.0', 'Pelayanan perkantoran', 0);
INSERT INTO `mst_kinerja` VALUES ('C.3', '3.1', 'Kantor lembaga memiliki konter pelayanan muzaki yang rapih resik dan ringkas', 1);
INSERT INTO `mst_kinerja` VALUES ('C.3', '3.2', 'Kantor lembaga memiliki konter pelayanan mustahik yang rapih resik dan ringkas', 1);
INSERT INTO `mst_kinerja` VALUES ('C.3', '3.3', 'Kantor Lembaga memiliki konter pelayanan mustahik dan muzakki yang terpisah', 1);
INSERT INTO `mst_kinerja` VALUES ('C.3', '3.4', 'Kantor lembaga memiliki penataan ruang untuk para pegawai dan peralatan perkantoran yang rapih resik ringkas ', 1);
INSERT INTO `mst_kinerja` VALUES ('C.3', '3.5', 'Kantor Lembaga memiliki tampak depan dan pelang nama kantor yang terlihat dari tepi jalan dengan logo, nama, huruf dan warna sesuai standar organisasi ', 1);
INSERT INTO `mst_kinerja` VALUES ('C.3', '3.6', 'Kantor lembaga memiliki papan pengumuman tentang jenis layanan yang diberikan beserta alur pelayanannya yang terlihat oleh muzaki/mustahik', 1);
INSERT INTO `mst_kinerja` VALUES ('C.3', '3.7', 'Kantor lembaga memiliki jam buka dan tutup pelayanan yang terpampang diluar kantor', 1);
INSERT INTO `mst_kinerja` VALUES ('C.3', '3.8', 'Kantor lembaga memiliki ruang tunggu yang layak bagi muzaki dan mustahik', 1);
INSERT INTO `mst_kinerja` VALUES ('C.3', '3.9', 'Kantor lembaga memiliki sistem keamanan kantor yang memadai', 1);
INSERT INTO `mst_kinerja` VALUES ('C.3', '3.10', 'Kantor lembaga memiliki ruang arsip yang memadai', 1);
INSERT INTO `mst_kinerja` VALUES ('C.3', '3.11', 'Kantor lembaga menerapkan penggunaan sumber daya energi (listrik, air, kertas) secara efisien dan efektif', 1);
INSERT INTO `mst_kinerja` VALUES ('C.3', '3.12', 'Pegawai kantor memakai seragam dan name tag sesuai dengan posisinya masing-masing dan memiliki papan nama pribadi di meja kerjanya', 1);
INSERT INTO `mst_kinerja` VALUES ('D.0', NULL, 'Kinerja Pengumpulan ', 0);
INSERT INTO `mst_kinerja` VALUES ('D.1', '1.0', 'Pertumbuhan capaian pengumpulan zakat', 0);
INSERT INTO `mst_kinerja` VALUES ('D.1', '1.1', 'Visi dan Misi Lembaga tercantum dengan jelas dan dapat dipahami oleh semua amil lembaga pengelola zakat', 1);
INSERT INTO `mst_kinerja` VALUES ('D.1', '1.2', 'Lembaga memiliki data cakupan potensial calon muzakki', 1);
INSERT INTO `mst_kinerja` VALUES ('D.1', '1.3', 'Lembaga melakukan evaluasi kerja secara berkala dalam mencapai target pencapaian pengumpulan', 1);
INSERT INTO `mst_kinerja` VALUES ('D.1', '1.4', 'Lembaga mengalami peningkatan dalam melakukan pengumpulan dana Zakat dan dana lainnya dalam 3 tahun terakhir', 1);
INSERT INTO `mst_kinerja` VALUES ('D.2', '2.0', 'Pertumbuhan data lengkap muzakki', 0);
INSERT INTO `mst_kinerja` VALUES ('D.2', '2.1', 'Lembaga memiliki data lengkap muzaki/munfik', 1);
INSERT INTO `mst_kinerja` VALUES ('D.2', '2.2', 'Lembaga memberikan laporan secara berkala untuk memaintenance/merawat muzaki agar tetap membayarkan Zakat, Infak dan Sedekahnya ke lembaga', 1);
INSERT INTO `mst_kinerja` VALUES ('D.2', '2.3', 'Lembaga memiliki data peningkatan kinerja berupa muzaki yang bertambah tiap tahun dalam 3 tahun terakhir', 1);
INSERT INTO `mst_kinerja` VALUES ('D.3', '3.0', 'SOP sesuai Fiqh Zakat dalam pengumpulan zakat', 0);
INSERT INTO `mst_kinerja` VALUES ('D.3', '3.1', 'Lembaga memiliki SOP dibidang pengumpulan zakat sesuai dengan fiqh zakat ', 1);
INSERT INTO `mst_kinerja` VALUES ('D.3', '3.2', 'Lembaga melaksanakan pengumpulan zakat sesuai dengan SOP dan Fiqh Zakat', 1);
INSERT INTO `mst_kinerja` VALUES ('D.4', '4.0', 'Pertumbuhan data unit di daerah (UPZ untuk BAZNAS)/ Cabang (Perwakilan Kantor Cabang Provinsi/Kota/Kab untuk LAZ yang telah diresmikan oleh pemerintah', 0);
INSERT INTO `mst_kinerja` VALUES ('D.4', '4.1', 'Pemerintah pusat/daerah di lingkungan lembaga telah mengeluarkan instruksi tentang kewajiban pembentukan UPZ di daerah (bagi BAZNAS) atau mendukung Cabang yang telah dibentuk oleh LAZ', 1);
INSERT INTO `mst_kinerja` VALUES ('D.4', '4.2', 'Lembaga melakukan sosialisasi pembentukan Unit/Cabang di SKPD Daerah , BUMD, perusahaan swasta/masyarakat berprofesi pns dan non pns', 1);
INSERT INTO `mst_kinerja` VALUES ('D.4', '4.3', 'Lembaga memiliki data pembentukan Unit/Cabang di Daerah', 1);
INSERT INTO `mst_kinerja` VALUES ('D.4', '4.4', 'Lembaga memiliki data potensial untuk pembentukan unit di daerah', 1);
INSERT INTO `mst_kinerja` VALUES ('D.5', '5.0', 'Program Kerja pengumpulan Zakat', 0);
INSERT INTO `mst_kinerja` VALUES ('D.5', '5.1', 'Lembaga memiliki program kerja pengumpulan zakat dalam 3 tahun terakhir', 1);
INSERT INTO `mst_kinerja` VALUES ('D.5', '5.2', 'Lembaga melakukan evaluasi pencapaian target pengumpulan secara berkala', 1);
INSERT INTO `mst_kinerja` VALUES ('D.6', '6.0', 'Pemberian Bukti Setor Zakat yang Sah kepada muzaki sebagai pengurang penghasilan kena pajak', 0);
INSERT INTO `mst_kinerja` VALUES ('D.6', '6.1', 'Lembaga memberikan bukti setor zakat ', 1);
INSERT INTO `mst_kinerja` VALUES ('D.6', '6.2', 'Format bukti setor zakat yang diberikan oleh lembaga mengedukasi muzaki tentang penghitungan harta dan prosentase zakat yang dikenakan', 1);
INSERT INTO `mst_kinerja` VALUES ('D.6', '6.3', 'Proses penerbitan bukti setor zakat menggunakan sistem elektronik', 1);
INSERT INTO `mst_kinerja` VALUES ('D.7', '7.0', 'Pemanfaatan media sebagai alat publikasi pengumpulan zakat', 0);
INSERT INTO `mst_kinerja` VALUES ('D.7', '7.1', 'Lembaga mempublikasikan kegiatan pendayagunaan zakat melalui media massa untuk menambah pengumpulan', 1);
INSERT INTO `mst_kinerja` VALUES ('D.7', '7.2', 'Lembaga menerapkan prinsip efisiensi dan efektivitas dalam memanfaatkan media massa untuk menambah pengumpulan', 1);
INSERT INTO `mst_kinerja` VALUES ('E.0', NULL, 'Kinerja Pendistribusian', 0);
INSERT INTO `mst_kinerja` VALUES ('E.1', '1.0', 'Program kerja pendayagunaan zakat', 0);
INSERT INTO `mst_kinerja` VALUES ('E.1', '1.1', 'Visi dan Misi Lembaga tercantum dengan jelas dan dapat dipahami oleh semua amil lembaga pengelola zakat', 2);
INSERT INTO `mst_kinerja` VALUES ('E.1', '1.2', 'Lembaga melakukan survei kelayakan kepada mustahik dalam menyalurkan dana ZIS', 2);
INSERT INTO `mst_kinerja` VALUES ('E.1', '1.3', 'Lembaga menerapkan kesesuaian prinsip syariat Islam dalam melakukan pendistribusian dana ZIS ( Hasil Audit Syariah dapat dijadikan pertimbangan oleh Tim Penilai)', 2);
INSERT INTO `mst_kinerja` VALUES ('E.1', '1.4', 'Lembaga melakukan evaluasi program kerja di bidang pendistribusian', 2);
INSERT INTO `mst_kinerja` VALUES ('E.2', '2.0', 'Data lengkap mustahik untuk program pendayagunaan', 0);
INSERT INTO `mst_kinerja` VALUES ('E.2', '2.1', 'Lembaga memiliki data mustahik untuk program pendayagunaan zakat', 2);
INSERT INTO `mst_kinerja` VALUES ('E.2', '2.2', 'Pendataan terpisah antara program pendistribusian dan program pendayagunaan', 2);
INSERT INTO `mst_kinerja` VALUES ('E.2', '2.3', 'Lembaga menginput data mustahik dan tercatat dalam sistem software zakat yang ada', 2);
INSERT INTO `mst_kinerja` VALUES ('E.3', '3.0', 'SOP Pemberdayaan Zakat', 0);
INSERT INTO `mst_kinerja` VALUES ('E.3', '3.1', 'Lembaga memiliki SOP dibidang pemberdayaan zakat', 2);
INSERT INTO `mst_kinerja` VALUES ('E.3', '3.2', 'Lembaga melaksanakan SOP dibidang pendayagunaan zakat secara konsisten', 2);
INSERT INTO `mst_kinerja` VALUES ('E.4', '4.0', 'Keberhasilan program pengentasan mustahik menjadi muzakki', 0);
INSERT INTO `mst_kinerja` VALUES ('E.4', '4.1', 'Lembaga memiliki data mustahik yang telah mandiri secara ekonomi dan statusnya menjadi non mustahik', 2);
INSERT INTO `mst_kinerja` VALUES ('E.4', '4.2', 'Lembaga memiliki data mustahik yang telah berhasil menjadi muzaki yang membayarkan zakatnya melalui lembaga ', 2);
INSERT INTO `mst_kinerja` VALUES ('E.4', '4.3', 'Lembaga memiliki data peningkatan kinerja dalam mengentaskan kemiskinan/pendayagunaan/pendistribusian dalam 3 tahun terakhir', 2);
INSERT INTO `mst_kinerja` VALUES ('E.5', '5.0', 'Laporan pendayagunaan kepada muzakki/ muzakki prospek/masyarakat', 0);
INSERT INTO `mst_kinerja` VALUES ('E.5', '5.1', 'Lembaga memberikan laporan hasil pendayagunaannya kepada muzakki/masyarakat secara rutin', 2);
INSERT INTO `mst_kinerja` VALUES ('E.5', '5.2', 'Laporan pendayagunaan telah dipublikasikan kepada masyarakat dengan menggunakan media elektronik dan media cetak', 2);

-- ----------------------------
-- Table structure for mst_syarat
-- ----------------------------
DROP TABLE IF EXISTS `mst_syarat`;
CREATE TABLE `mst_syarat`  (
  `kode` char(8) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `nama` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mst_syarat
-- ----------------------------
INSERT INTO `mst_syarat` VALUES ('syarat01', 'Memiliki Badan Hukum SAH');
INSERT INTO `mst_syarat` VALUES ('syarat02', 'Surat Izin Pendirian Lembaga Pengelola Zakat');
INSERT INTO `mst_syarat` VALUES ('syarat03', 'Rekomendasi BAZNAS');
INSERT INTO `mst_syarat` VALUES ('syarat04', 'Daftar Donatur/Muzaki tetap');
INSERT INTO `mst_syarat` VALUES ('syarat05', 'Daftar Mustahik tetap');
INSERT INTO `mst_syarat` VALUES ('syarat06', 'Memiliki Sarana dan Prasarana');
INSERT INTO `mst_syarat` VALUES ('syarat07', 'Memiliki struktur kepengurusan /tenaga kerja tetap');
INSERT INTO `mst_syarat` VALUES ('syarat08', 'Melaksanakan pengumpulan dana zakat dan pendistribusian dana zakat');
INSERT INTO `mst_syarat` VALUES ('syarat09', 'Memiliki program kerja');
INSERT INTO `mst_syarat` VALUES ('syarat10', 'Memiliki pembukuan yang jelas');
INSERT INTO `mst_syarat` VALUES ('syarat11', 'Melampirkan surat pernyataan bersedia untuk diakreditasi');
INSERT INTO `mst_syarat` VALUES ('syarat12', 'Memiliki program untuk mendayagunakan zakat bagi kesejahteraan masyarakat');

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets`  (
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  INDEX `password_resets_email_index`(`email`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for pbcatcol
-- ----------------------------
DROP TABLE IF EXISTS `pbcatcol`;
CREATE TABLE `pbcatcol`  (
  `pbc_tnam` char(193) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `pbc_tid` int(11) NULL DEFAULT NULL,
  `pbc_ownr` char(193) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `pbc_cnam` char(193) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `pbc_cid` smallint(6) NULL DEFAULT NULL,
  `pbc_labl` varchar(254) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `pbc_lpos` smallint(6) NULL DEFAULT NULL,
  `pbc_hdr` varchar(254) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `pbc_hpos` smallint(6) NULL DEFAULT NULL,
  `pbc_jtfy` smallint(6) NULL DEFAULT NULL,
  `pbc_mask` varchar(31) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `pbc_case` smallint(6) NULL DEFAULT NULL,
  `pbc_hght` smallint(6) NULL DEFAULT NULL,
  `pbc_wdth` smallint(6) NULL DEFAULT NULL,
  `pbc_ptrn` varchar(31) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `pbc_bmap` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `pbc_init` varchar(254) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `pbc_cmnt` varchar(254) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `pbc_edit` varchar(31) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `pbc_tag` varchar(254) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  UNIQUE INDEX `pbcatc_x`(`pbc_tnam`, `pbc_ownr`, `pbc_cnam`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pbcatcol
-- ----------------------------

-- ----------------------------
-- Table structure for pbcatedt
-- ----------------------------
DROP TABLE IF EXISTS `pbcatedt`;
CREATE TABLE `pbcatedt`  (
  `pbe_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `pbe_edit` varchar(254) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `pbe_type` smallint(6) NULL DEFAULT NULL,
  `pbe_cntr` int(11) NULL DEFAULT NULL,
  `pbe_seqn` smallint(6) NOT NULL,
  `pbe_flag` int(11) NULL DEFAULT NULL,
  `pbe_work` char(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  UNIQUE INDEX `pbcate_x`(`pbe_name`, `pbe_seqn`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pbcatedt
-- ----------------------------
INSERT INTO `pbcatedt` VALUES ('###-##-####', '###-##-####', 90, 1, 1, 32, '00');
INSERT INTO `pbcatedt` VALUES ('###,###.00', '###,###.00', 90, 1, 1, 32, '10');
INSERT INTO `pbcatedt` VALUES ('#####', '#####', 90, 1, 1, 32, '10');
INSERT INTO `pbcatedt` VALUES ('DD/MM/YY', 'DD/MM/YY', 90, 1, 1, 32, '20');
INSERT INTO `pbcatedt` VALUES ('DD/MM/YY HH:MM:SS', 'DD/MM/YY HH:MM:SS', 90, 1, 1, 32, '40');
INSERT INTO `pbcatedt` VALUES ('DD/MM/YY HH:MM:SS:FFFFFF', 'DD/MM/YY HH:MM:SS:FFFFFF', 90, 1, 1, 32, '40');
INSERT INTO `pbcatedt` VALUES ('DD/MM/YYYY', 'DD/MM/YYYY', 90, 1, 1, 32, '20');
INSERT INTO `pbcatedt` VALUES ('DD/MM/YYYY HH:MM:SS', 'DD/MM/YYYY HH:MM:SS', 90, 1, 1, 32, '40');
INSERT INTO `pbcatedt` VALUES ('DD/MMM/YY', 'DD/MMM/YY', 90, 1, 1, 32, '20');
INSERT INTO `pbcatedt` VALUES ('DD/MMM/YY HH:MM:SS', 'DD/MMM/YY HH:MM:SS', 90, 1, 1, 32, '40');
INSERT INTO `pbcatedt` VALUES ('HH:MM:SS', 'HH:MM:SS', 90, 1, 1, 32, '30');
INSERT INTO `pbcatedt` VALUES ('HH:MM:SS:FFF', 'HH:MM:SS:FFF', 90, 1, 1, 32, '30');
INSERT INTO `pbcatedt` VALUES ('HH:MM:SS:FFFFFF', 'HH:MM:SS:FFFFFF', 90, 1, 1, 32, '30');
INSERT INTO `pbcatedt` VALUES ('JJJ/YY', 'JJJ/YY', 90, 1, 1, 32, '20');
INSERT INTO `pbcatedt` VALUES ('JJJ/YY HH:MM:SS', 'JJJ/YY HH:MM:SS', 90, 1, 1, 32, '40');
INSERT INTO `pbcatedt` VALUES ('JJJ/YYYY', 'JJJ/YYYY', 90, 1, 1, 32, '20');
INSERT INTO `pbcatedt` VALUES ('JJJ/YYYY HH:MM:SS', 'JJJ/YYYY HH:MM:SS', 90, 1, 1, 32, '40');
INSERT INTO `pbcatedt` VALUES ('MM/DD/YY', 'MM/DD/YY', 90, 1, 1, 32, '20');
INSERT INTO `pbcatedt` VALUES ('MM/DD/YY HH:MM:SS', 'MM/DD/YY HH:MM:SS', 90, 1, 1, 32, '40');
INSERT INTO `pbcatedt` VALUES ('MM/DD/YYYY', 'MM/DD/YYYY', 90, 1, 1, 32, '20');
INSERT INTO `pbcatedt` VALUES ('MM/DD/YYYY HH:MM:SS', 'MM/DD/YYYY HH:MM:SS', 90, 1, 1, 32, '40');

-- ----------------------------
-- Table structure for pbcatfmt
-- ----------------------------
DROP TABLE IF EXISTS `pbcatfmt`;
CREATE TABLE `pbcatfmt`  (
  `pbf_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `pbf_frmt` varchar(254) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `pbf_type` smallint(6) NULL DEFAULT NULL,
  `pbf_cntr` int(11) NULL DEFAULT NULL,
  UNIQUE INDEX `pbcatf_x`(`pbf_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pbcatfmt
-- ----------------------------
INSERT INTO `pbcatfmt` VALUES ('[General]', '[General]', 81, 0);
INSERT INTO `pbcatfmt` VALUES ('#,##0', '#,##0', 81, 0);
INSERT INTO `pbcatfmt` VALUES ('#,##0.00', '#,##0.00', 81, 0);
INSERT INTO `pbcatfmt` VALUES ('$#,##0;($#,##0)', '$#,##0;($#,##0)', 81, 0);
INSERT INTO `pbcatfmt` VALUES ('$#,##0;[RED]($#,##0)', '$#,##0;[RED]($#,##0)', 81, 0);
INSERT INTO `pbcatfmt` VALUES ('$#,##0.00;($#,##0.00)', '$#,##0.00;($#,##0.00)', 81, 0);
INSERT INTO `pbcatfmt` VALUES ('$#,##0.00;[RED]($#,##0.00)', '$#,##0.00;[RED]($#,##0.00)', 81, 0);
INSERT INTO `pbcatfmt` VALUES ('0', '0', 81, 0);
INSERT INTO `pbcatfmt` VALUES ('0.00', '0.00', 81, 0);
INSERT INTO `pbcatfmt` VALUES ('0.00%', '0.00%', 81, 0);
INSERT INTO `pbcatfmt` VALUES ('0.00E+00', '0.00E+00', 81, 0);
INSERT INTO `pbcatfmt` VALUES ('0%', '0%', 81, 0);
INSERT INTO `pbcatfmt` VALUES ('d-mmm', 'd-mmm', 84, 0);
INSERT INTO `pbcatfmt` VALUES ('d-mmm-yy', 'd-mmm-yy', 84, 0);
INSERT INTO `pbcatfmt` VALUES ('h:mm AM/PM', 'h:mm AM/PM', 84, 0);
INSERT INTO `pbcatfmt` VALUES ('h:mm:ss', 'h:mm:ss', 84, 0);
INSERT INTO `pbcatfmt` VALUES ('h:mm:ss AM/PM', 'h:mm:ss AM/PM', 84, 0);
INSERT INTO `pbcatfmt` VALUES ('m/d/yy', 'm/d/yy', 84, 0);
INSERT INTO `pbcatfmt` VALUES ('m/d/yy h:mm', 'm/d/yy h:mm', 84, 0);
INSERT INTO `pbcatfmt` VALUES ('mmm-yy', 'mmm-yy', 84, 0);

-- ----------------------------
-- Table structure for pbcattbl
-- ----------------------------
DROP TABLE IF EXISTS `pbcattbl`;
CREATE TABLE `pbcattbl`  (
  `pbt_tnam` char(193) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `pbt_tid` int(11) NULL DEFAULT NULL,
  `pbt_ownr` char(193) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `pbd_fhgt` smallint(6) NULL DEFAULT NULL,
  `pbd_fwgt` smallint(6) NULL DEFAULT NULL,
  `pbd_fitl` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `pbd_funl` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `pbd_fchr` smallint(6) NULL DEFAULT NULL,
  `pbd_fptc` smallint(6) NULL DEFAULT NULL,
  `pbd_ffce` char(18) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `pbh_fhgt` smallint(6) NULL DEFAULT NULL,
  `pbh_fwgt` smallint(6) NULL DEFAULT NULL,
  `pbh_fitl` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `pbh_funl` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `pbh_fchr` smallint(6) NULL DEFAULT NULL,
  `pbh_fptc` smallint(6) NULL DEFAULT NULL,
  `pbh_ffce` char(18) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `pbl_fhgt` smallint(6) NULL DEFAULT NULL,
  `pbl_fwgt` smallint(6) NULL DEFAULT NULL,
  `pbl_fitl` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `pbl_funl` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `pbl_fchr` smallint(6) NULL DEFAULT NULL,
  `pbl_fptc` smallint(6) NULL DEFAULT NULL,
  `pbl_ffce` char(18) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `pbt_cmnt` varchar(254) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  UNIQUE INDEX `pbcatt_x`(`pbt_tnam`, `pbt_ownr`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pbcattbl
-- ----------------------------

-- ----------------------------
-- Table structure for pbcatvld
-- ----------------------------
DROP TABLE IF EXISTS `pbcatvld`;
CREATE TABLE `pbcatvld`  (
  `pbv_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `pbv_vald` varchar(254) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `pbv_type` smallint(6) NULL DEFAULT NULL,
  `pbv_cntr` int(11) NULL DEFAULT NULL,
  `pbv_msg` varchar(254) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  UNIQUE INDEX `pbcatv_x`(`pbv_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pbcatvld
-- ----------------------------

-- ----------------------------
-- Table structure for pengaduan
-- ----------------------------
DROP TABLE IF EXISTS `pengaduan`;
CREATE TABLE `pengaduan`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `no_hp` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `keterangan` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NOT NULL DEFAULT current_timestamp ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime(0) NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pengaduan
-- ----------------------------
INSERT INTO `pengaduan` VALUES (7, 'A Wafa Hermawan', 'ahmadwafa662@gmail.com', '085784516803', 'saran dan kritikan dari saya adalah bla bla bla', '2020-08-22 12:25:10', '2020-08-22 12:25:10');

-- ----------------------------
-- Table structure for permission
-- ----------------------------
DROP TABLE IF EXISTS `permission`;
CREATE TABLE `permission`  (
  `id` int(122) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_type` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `data` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of permission
-- ----------------------------
INSERT INTO `permission` VALUES (1, 'Member', '{\"users\":{\"own_create\":\"1\",\"own_read\":\"1\",\"own_update\":\"1\",\"own_delete\":\"1\"}}');
INSERT INTO `permission` VALUES (2, 'admin', '{\"users\":{\"own_create\":\"1\",\"own_read\":\"1\",\"own_update\":\"1\",\"own_delete\":\"1\",\"all_create\":\"1\",\"all_read\":\"1\",\"all_update\":\"1\",\"all_delete\":\"1\"}}');

-- ----------------------------
-- Table structure for setting
-- ----------------------------
DROP TABLE IF EXISTS `setting`;
CREATE TABLE `setting`  (
  `id` int(122) UNSIGNED NOT NULL AUTO_INCREMENT,
  `keys` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `value` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 23 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of setting
-- ----------------------------
INSERT INTO `setting` VALUES (1, 'website', 'LPZ- Lembaga Amil Zakat');
INSERT INTO `setting` VALUES (2, 'logo', 'SIMZAT_1508987274.png');
INSERT INTO `setting` VALUES (3, 'favicon', '');
INSERT INTO `setting` VALUES (4, 'SMTP_EMAIL', 'lpzinformasi@gmail.com');
INSERT INTO `setting` VALUES (5, 'HOST', 'smtp.gmail.com');
INSERT INTO `setting` VALUES (6, 'PORT', '465');
INSERT INTO `setting` VALUES (7, 'SMTP_SECURE', 'ssl');
INSERT INTO `setting` VALUES (8, 'SMTP_PASSWORD', 'Lpzzakat123');
INSERT INTO `setting` VALUES (9, 'mail_setting', 'php_mailer');
INSERT INTO `setting` VALUES (10, 'company_name', 'LPZ- Lembaga Amil Zakat');
INSERT INTO `setting` VALUES (11, 'crud_list', 'users,User');
INSERT INTO `setting` VALUES (12, 'EMAIL', 'lpzinformasi@gmail.com');
INSERT INTO `setting` VALUES (13, 'UserModules', 'yes');
INSERT INTO `setting` VALUES (14, 'register_allowed', '1');
INSERT INTO `setting` VALUES (15, 'email_invitation', '1');
INSERT INTO `setting` VALUES (16, 'admin_approval', '0');
INSERT INTO `setting` VALUES (17, 'user_type', '[\"Member\"]');
INSERT INTO `setting` VALUES (18, 'slide1', '');
INSERT INTO `setting` VALUES (19, 'slide2', '');
INSERT INTO `setting` VALUES (20, 'slide3', '');
INSERT INTO `setting` VALUES (22, 'data_body', 'Lorem ipsum dolor sit amet, augue ponderum eum ei, ne eum appetere verterem necessitatibus. Cu oratio voluptua consequat sea, iusto libris suavitate sed ne.');
INSERT INTO `setting` VALUES (21, 'data_title', 'Lorem Ipsums');

-- ----------------------------
-- Table structure for settmail
-- ----------------------------
DROP TABLE IF EXISTS `settmail`;
CREATE TABLE `settmail`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `protocol` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `smtp_host` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `smtp_port` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `smtp_user` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `smtp_pass` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `mailtype` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT 'html',
  `charset` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT 'utf8',
  `template` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of settmail
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_nerca
-- ----------------------------
DROP TABLE IF EXISTS `tbl_nerca`;
CREATE TABLE `tbl_nerca`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `periode` char(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `zakat` decimal(18, 2) NULL DEFAULT NULL,
  `infaq` decimal(18, 2) NULL DEFAULT NULL,
  `dansos_lain` decimal(18, 2) NULL DEFAULT NULL,
  `total_peroleh` decimal(18, 2) NULL DEFAULT NULL,
  `konsumtif` decimal(18, 2) NULL DEFAULT NULL,
  `produktif` decimal(18, 2) NULL DEFAULT NULL,
  `total_distribusi` decimal(18, 2) NULL DEFAULT NULL,
  `saldo` decimal(18, 2) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbl_nerca
-- ----------------------------
INSERT INTO `tbl_nerca` VALUES (1, '6', 'Oct-17', 10000000.00, 12500000.00, 1000000.00, 23500000.00, 125000.00, 765000.00, 890000.00, 22610000.00);
INSERT INTO `tbl_nerca` VALUES (2, '15', 'Oct-17', 100.00, 200.00, 300.00, 600.00, 300.00, 300.00, 600.00, 0.00);
INSERT INTO `tbl_nerca` VALUES (3, '15', 'Aug-17', 300000.00, 10000.00, 10000.00, 320000.00, 100000.00, 20000.00, 120000.00, 200000.00);
INSERT INTO `tbl_nerca` VALUES (4, '15', 'Sep-17', 300.00, 4000.00, 7900.00, 12200.00, 500.00, 300.00, 800.00, 11400.00);
INSERT INTO `tbl_nerca` VALUES (5, '50', 'Nov-17', 20000000.00, 3000000.00, 300000.00, 23300000.00, 400000.00, 400000.00, 800000.00, 22500000.00);
INSERT INTO `tbl_nerca` VALUES (6, '50', 'Dec-17', 40000000.00, 5000000.00, 50000000.00, 95000000.00, 50000000.00, 5000000.00, 55000000.00, 40000000.00);
INSERT INTO `tbl_nerca` VALUES (7, '18', 'Jan-17', 15000.00, 300.00, 200.00, 15500.00, 500.00, 1400.00, 1900.00, 13600.00);
INSERT INTO `tbl_nerca` VALUES (8, '18', 'Feb-17', 544456.00, 55156.00, 41546.00, 641158.00, 12163.00, 212.00, 12375.00, 628783.00);
INSERT INTO `tbl_nerca` VALUES (13, '16', 'Aug-20', 2000000.00, 4500000.00, 300000.00, 6800000.00, 100000.00, 150000.00, 250000.00, 6550000.00);
INSERT INTO `tbl_nerca` VALUES (14, '7', 'Aug-20', 1000000.00, 340000.00, 900000.00, 2240000.00, 320000.00, 500000.00, 820000.00, 1420000.00);

-- ----------------------------
-- Table structure for templates
-- ----------------------------
DROP TABLE IF EXISTS `templates`;
CREATE TABLE `templates`  (
  `id` int(121) UNSIGNED NOT NULL AUTO_INCREMENT,
  `module` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `code` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `template_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `html` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of templates
-- ----------------------------
INSERT INTO `templates` VALUES (1, 'forgot_pass', 'forgot_password', 'Forgot password', '<html xmlns=\"http://www.w3.org/1999/xhtml\"><head>\n  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n  <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n  <style type=\"text/css\" rel=\"stylesheet\" media=\"all\">\n    /* Base ------------------------------ */\n    *:not(br):not(tr):not(html) {\n      font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif;\n      -webkit-box-sizing: border-box;\n      box-sizing: border-box;\n    }\n    body {\n      \n    }\n    a {\n      color: #3869D4;\n    }\n\n\n    /* Masthead ----------------------- */\n    .email-masthead {\n      padding: 25px 0;\n      text-align: center;\n    }\n    .email-masthead_logo {\n      max-width: 400px;\n      border: 0;\n    }\n    .email-footer {\n      width: 570px;\n      margin: 0 auto;\n      padding: 0;\n      text-align: center;\n    }\n    .email-footer p {\n      color: #AEAEAE;\n    }\n  \n    .content-cell {\n      padding: 35px;\n    }\n    .align-right {\n      text-align: right;\n    }\n\n    /* Type ------------------------------ */\n    h1 {\n      margin-top: 0;\n      color: #2F3133;\n      font-size: 19px;\n      font-weight: bold;\n      text-align: left;\n    }\n    h2 {\n      margin-top: 0;\n      color: #2F3133;\n      font-size: 16px;\n      font-weight: bold;\n      text-align: left;\n    }\n    h3 {\n      margin-top: 0;\n      color: #2F3133;\n      font-size: 14px;\n      font-weight: bold;\n      text-align: left;\n    }\n    p {\n      margin-top: 0;\n      color: #74787E;\n      font-size: 16px;\n      line-height: 1.5em;\n      text-align: left;\n    }\n    p.sub {\n      font-size: 12px;\n    }\n    p.center {\n      text-align: center;\n    }\n\n    /* Buttons ------------------------------ */\n    .button {\n      display: inline-block;\n      width: 200px;\n      background-color: #3869D4;\n      border-radius: 3px;\n      color: #ffffff;\n      font-size: 15px;\n      line-height: 45px;\n      text-align: center;\n      text-decoration: none;\n      -webkit-text-size-adjust: none;\n      mso-hide: all;\n    }\n    .button--green {\n      background-color: #22BC66;\n    }\n    .button--red {\n      background-color: #dc4d2f;\n    }\n    .button--blue {\n      background-color: #3869D4;\n    }\n  </style>\n</head>\n<body style=\"width: 100% !important;\n      height: 100%;\n      margin: 0;\n      line-height: 1.4;\n      background-color: #F2F4F6;\n      color: #74787E;\n      -webkit-text-size-adjust: none;\">\n  <table class=\"email-wrapper\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"\n    width: 100%;\n    margin: 0;\n    padding: 0;\">\n    <tbody><tr>\n      <td align=\"center\">\n        <table class=\"email-content\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%;\n      margin: 0;\n      padding: 0;\">\n          <!-- Logo -->\n\n          <tbody>\n          <!-- Email Body -->\n          <tr>\n            <td class=\"email-body\" width=\"100%\" style=\"width: 100%;\n    margin: 0;\n    padding: 0;\n    border-top: 1px solid #edeef2;\n    border-bottom: 1px solid #edeef2;\n    background-color: #edeef2;\">\n              <table class=\"email-body_inner\" align=\"center\" width=\"570\" cellpadding=\"0\" cellspacing=\"0\" style=\" width: 570px;\n    margin:  14px auto;\n    background: #fff;\n    padding: 0;\n    border: 1px outset rgba(136, 131, 131, 0.26);\n    box-shadow: 0px 6px 38px rgb(0, 0, 0);\n       \">\n                <!-- Body content -->\n                <thead style=\"background: #3869d4;\"><tr><th><div align=\"center\" style=\"padding: 15px; color: #000;\"><a href=\"{var_action_url}\" class=\"email-masthead_name\" style=\"font-size: 16px;\n      font-weight: bold;\n      color: #bbbfc3;\n      text-decoration: none;\n      text-shadow: 0 1px 0 white;\">{var_sender_name}</a></div></th></tr>\n                </thead>\n                <tbody><tr>\n                  <td class=\"content-cell\" style=\"padding: 35px;\">\n                    <h1>Hi {var_user_name},</h1>\n                    <p>You recently requested to reset your password for your {var_website_name} account. Click the button below to reset it.</p>\n                    <!-- Action -->\n                    <table class=\"body-action\" align=\"center\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"\n      width: 100%;\n      margin: 30px auto;\n      padding: 0;\n      text-align: center;\">\n                      <tbody><tr>\n                        <td align=\"center\">\n                          <div>\n                            <!--[if mso]><v:roundrect xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:w=\"urn:schemas-microsoft-com:office:word\" href=\"{{var_action_url}}\" style=\"height:45px;v-text-anchor:middle;width:200px;\" arcsize=\"7%\" stroke=\"f\" fill=\"t\">\n                              <v:fill type=\"tile\" color=\"#dc4d2f\" />\n                              <w:anchorlock/>\n                              <center style=\"color:#ffffff;font-family:sans-serif;font-size:15px;\">Reset your password</center>\n                            </v:roundrect><![endif]-->\n                            <a href=\"{var_varification_link}\" class=\"button button--red\" style=\"background-color: #dc4d2f;display: inline-block;\n      width: 200px;\n      background-color: #3869D4;\n      border-radius: 3px;\n      color: #ffffff;\n      font-size: 15px;\n      line-height: 45px;\n      text-align: center;\n      text-decoration: none;\n      -webkit-text-size-adjust: none;\n      mso-hide: all;\">Reset your password</a>\n                          </div>\n                        </td>\n                      </tr>\n                    </tbody></table>\n                    <p>If you did not request a password reset, please ignore this email or reply to let us know.</p>\n                    <p>Thanks,<br>{var_sender_name} and the {var_website_name} Team</p>\n                   <!-- Sub copy -->\n                    <table class=\"body-sub\" style=\"margin-top: 25px;\n      padding-top: 25px;\n      border-top: 1px solid #EDEFF2;\">\n                      <tbody><tr>\n                        <td> \n                          <p class=\"sub\" style=\"font-size:12px;\">If you are having trouble clicking the password reset button, copy and paste the URL below into your web browser.</p>\n                          <p class=\"sub\"  style=\"font-size:12px;\"><a href=\"{var_varification_link}\">{var_varification_link}</a></p>\n                        </td>\n                      </tr>\n                    </tbody></table>\n                  </td>\n                </tr>\n              </tbody></table>\n            </td>\n          </tr>\n        </tbody></table>\n      </td>\n    </tr>\n  </tbody></table>\n\n\n</body></html>');
INSERT INTO `templates` VALUES (3, 'users', 'invitation', 'Invitation', '<p>Hello <strong>{var_user_email}</strong></p>\n\n<p>Click below link to register&nbsp;<br />\n{var_inviation_link}</p>\n\n<p>Thanks&nbsp;</p>\n');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `var_key` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `is_deleted` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '0',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `tgl_pma` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `profile_pic` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `user_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `no_hp` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `alamat` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `no_telp` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `website` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `muzakki` int(11) NULL DEFAULT 0,
  `mustahik` int(11) NULL DEFAULT 0,
  `lat` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `lng` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `syarat01` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `syarat01_filename` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `syarat02` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `syarat02_filename` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `syarat03` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `syarat03_filename` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `syarat04` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `syarat04_filename` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `syarat05` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `syarat05_filename` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `syarat06` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `syarat06_filename` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `syarat07` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `syarat07_filename` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `syarat08` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `syarat08_filename` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `syarat09` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `syarat09_filename` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `syarat10` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `syarat10_filename` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `syarat11` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `syarat11_filename` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `syarat12` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `syarat12_filename` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `syarat13` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `syarat13_filename` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `verified` varchar(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`, `email`) USING BTREE,
  UNIQUE INDEX `email`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 69 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, '1', NULL, 'active', '0', 'Admin Utama', '$2y$10$E3eCHnD2R3tp0zqDYnFEnOG8PMSYbS2syubOMhbA4q0UKlUiIfZ0O', 'admin@admin.com', '', '_1508730106.', 'admin', '09823174198', 'fsdfgas jhgjd jgjh', '031732487', '', 0, 0, '-7.276060', '112.791741', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-08-30 14:48:33', 'Y');
INSERT INTO `users` VALUES (15, '15', '', 'active', '0', 'LAZ Yayasan Rumah Zakat Indonesia (LAZ RZ)', '$2y$10$qUc0wGX.KAQozT69rqsrhua3Z4xbPkCbbb0ibB3mrVkddBKx4xKL.', 'lazrz@zakat.com', '421 Tahun 2015, 30 Desember tahun 2015', '', 'LAZ', '1', '1', NULL, '1', 0, 0, '-6.298665', '106.819208', '1', '15_syarat01_171029142719.pdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-09-01 12:45:24', 'Y');
INSERT INTO `users` VALUES (16, '16', '', 'active', '0', 'LAZ Yayasan Nurul Hayat (LAZ NH)', '$2y$10$WlUspvBnyr8bTvnVQ1Fd..RR/0wHq3uiOBrOO.AT97cBmRMQwjHxW', 'laznh@zakat.com', '422 Tahun 2015, 30 Desember Tahun 2015', '', 'LAZ', NULL, NULL, NULL, '', 0, 0, '-2.985481', '104.764335', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N');
INSERT INTO `users` VALUES (17, '17', '', 'active', '0', 'LAZ Inisiatif Zakat Indonesia (LAZ IZI)', '$2y$10$WlUspvBnyr8bTvnVQ1Fd..RR/0wHq3uiOBrOO.AT97cBmRMQwjHxW', 'lazbmh@zakat.com', '423 Tahun 2015, 30 Desember 2015', '', 'BAZ', NULL, NULL, NULL, 'www.izi.or.id', 65482, 120560, '4.821892', '96.817960', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N');
INSERT INTO `users` VALUES (18, '18', '', 'active', '0', 'LAZ Baitul Maal Hidayatullah (LAZ BMH)', '$2y$10$WlUspvBnyr8bTvnVQ1Fd..RR/0wHq3uiOBrOO.AT97cBmRMQwjHxW', 'lazlmi@zakat.com', '425 tahun 2015, 30 Desember tahun 2015', '', 'BAZ', NULL, NULL, NULL, '', 0, 0, '3.759918', '116.746805', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N');
INSERT INTO `users` VALUES (19, '19', '', 'active', '0', 'Yayasan Lembaga Manajemen Infaq (LAZ LMI)', '$2y$10$WlUspvBnyr8bTvnVQ1Fd..RR/0wHq3uiOBrOO.AT97cBmRMQwjHxW', 'lazym@zakat.com', '184 Tahun 2016, 29 April 2016', '', 'BAZ', NULL, NULL, NULL, '', 0, 0, '1.554542', '124.926277', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N');
INSERT INTO `users` VALUES (20, '20', '', 'active', '0', 'Yayasan Yatim Mandiri Surabaya (LAZ Yatim Mandiri)', '$2y$10$WlUspvBnyr8bTvnVQ1Fd..RR/0wHq3uiOBrOO.AT97cBmRMQwjHxW', 'lazdd@zakat.com', '185 tahun 2016. 29 April Tahun 2016', '', 'BAZ', NULL, NULL, NULL, '', 0, 0, '2.368815', '128.425285', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N');
INSERT INTO `users` VALUES (21, '21', '', 'active', '0', 'LAZ Yayasan Dompet Dhuafa Republika (LAZ DD)', '$2y$10$WlUspvBnyr8bTvnVQ1Fd..RR/0wHq3uiOBrOO.AT97cBmRMQwjHxW', 'lazaz@zakat.com', '239 tahun 2016, 23 Mei 2016', '', 'BAZ', NULL, NULL, NULL, '', 0, 0, '-2.705812', '134.593503', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N');
INSERT INTO `users` VALUES (22, '22', '', 'active', '0', 'Yayasan Pesantren Islam al-Azhar (LAZ Al- Azhar)', '$2y$10$WlUspvBnyr8bTvnVQ1Fd..RR/0wHq3uiOBrOO.AT97cBmRMQwjHxW', 'lazdt@zakat.com', '240 Tahun 2016, 23 Mei tahun 2016', '', 'BAZ', NULL, NULL, NULL, '', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N');
INSERT INTO `users` VALUES (24, '24', '', 'active', '0', 'Yayasan Lembaga Amil Infaq dan Shadaqah Nahdlatul Ulama (LAZIS NU)', '$2y$10$WlUspvBnyr8bTvnVQ1Fd..RR/0wHq3uiOBrOO.AT97cBmRMQwjHxW', 'lazisnu@zakat.com', '255 Tahun 2016, 26 Mei 2016', '', 'BAZ', NULL, NULL, NULL, '', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N');
INSERT INTO `users` VALUES (25, '25', '', 'active', '0', 'LAZ Yayasan Baitulmaal Muamalat (LAZ BMM)', '$2y$10$WlUspvBnyr8bTvnVQ1Fd..RR/0wHq3uiOBrOO.AT97cBmRMQwjHxW', 'lazbmm@zakat.com', '256 Tahun 2016, 26 Mei 2016', '', 'LAZ', NULL, NULL, NULL, '', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N');
INSERT INTO `users` VALUES (26, '26', '', 'active', '0', 'LAZ Yayasan Dana Sosial Al Falah (LAZ YDSF)', '$2y$10$WlUspvBnyr8bTvnVQ1Fd..RR/0wHq3uiOBrOO.AT97cBmRMQwjHxW', 'lazydsf@zakat.com', 'KMA No. 524 Tahun 2016 Tanggal 20 September 2016', '', 'LAZ', NULL, NULL, NULL, '', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N');
INSERT INTO `users` VALUES (28, '28', '', 'active', '0', 'LAZIS Muhammadiyah', '$2y$10$WlUspvBnyr8bTvnVQ1Fd..RR/0wHq3uiOBrOO.AT97cBmRMQwjHxW', 'lazismu@zakat.com', 'Keputusan Menteri Agama No. 730 tahun 2016 Tanggal 14 Desember 2016', '', 'LAZ', NULL, NULL, NULL, '', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N');
INSERT INTO `users` VALUES (29, '29', '', 'active', '0', 'Yayasan Global Zakat BBI', '$2y$10$WlUspvBnyr8bTvnVQ1Fd..RR/0wHq3uiOBrOO.AT97cBmRMQwjHxW', 'lazgz@zakat.com', 'KMA No. 731 Tahun 2016 Tanggal 14 Desember 2016', '', 'BBI', NULL, NULL, NULL, '', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N');
INSERT INTO `users` VALUES (30, '30', '', 'active', '0', 'LAZ Perkumpulan Persatuan Islam (PERSIS)', '$2y$10$WlUspvBnyr8bTvnVQ1Fd..RR/0wHq3uiOBrOO.AT97cBmRMQwjHxW', 'lazper@zakat.com', 'Keputusan Menteri Agama No. 865 Tahun 2016 Tanggal 30 Desember 2016', '', 'Member', NULL, NULL, NULL, '', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N');
INSERT INTO `users` VALUES (31, '31', '', 'active', '0', 'LAZ Rumah Yatim Arrohman Indonesia', '$2y$10$WlUspvBnyr8bTvnVQ1Fd..RR/0wHq3uiOBrOO.AT97cBmRMQwjHxW', 'lazyai@zakat.com', '', '', 'Member', NULL, NULL, NULL, '', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N');
INSERT INTO `users` VALUES (32, '32', NULL, 'active', '0', 'Yayasan Solo Peduli Umat (LAZ Solo Peduli)', '$2y$10$WlUspvBnyr8bTvnVQ1Fd..RR/0wHq3uiOBrOO.AT97cBmRMQwjHxW', 'lazsol@zakat.com', 'Keputusan Dirjen Bimas Islam No. Dj.III/271 Tahun 2016 Tanggal 14 April 2016', NULL, 'Member', NULL, 'Jl. Adi sucipto No. 190 Solo Surakarta Jawa Tengah', NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N');
INSERT INTO `users` VALUES (33, '33', NULL, 'active', '0', 'Yayasan Baitul Maal Forum Komunikasi Aktifis Masjid (LAZ FKAM)', '$2y$10$WlUspvBnyr8bTvnVQ1Fd..RR/0wHq3uiOBrOO.AT97cBmRMQwjHxW', 'lazfkam@zakat.com', 'Keputusan Dirjen Bimas Islam No. Dj.III/271 Tahun 2016 Tanggal 7 Juni 2016', NULL, 'Member', NULL, 'Jl. Slamet Riyadi No. 287 Sriwedari Laweyan Surakarta', NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N');
INSERT INTO `users` VALUES (34, '34', NULL, 'active', '0', 'Yayasan Dompet Amal Sejahtera Ibnu  Abbas', '$2y$10$WlUspvBnyr8bTvnVQ1Fd..RR/0wHq3uiOBrOO.AT97cBmRMQwjHxW', 'lazdasib@zakat.com', 'Keputusan Dirjen Bimas Islam No. Dj.III/391 Tahun 2016 Tanggal 7 Juni 2016', NULL, 'Member', NULL, 'Jl. Bung karno No. 76 B Pegangsaan Timur Kecamatan Mataram Kota Mataram NTB', NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N');
INSERT INTO `users` VALUES (35, '35', NULL, 'active', '0', 'Yayasan Dana Peduli Ummat (DPU) Kalimantan Timur', '$2y$10$WlUspvBnyr8bTvnVQ1Fd..RR/0wHq3uiOBrOO.AT97cBmRMQwjHxW', 'lazdpu@zakat.com', 'Keputusan Dirjen Bimas Islam No. Dj.III/515 Tahun 2016 Tanggal 24 Agustus 2016', NULL, 'Member', NULL, 'Jl. Siradj Salman Ruko Grand Mutiara Blok C Nomor 3 Teluk Lerong Ilir Samarinda Ulu Kota Samarinda Kalimantan Timur', NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N');
INSERT INTO `users` VALUES (36, '36', NULL, 'active', '0', 'Yayasan Dhompet Sosial Madani (LAZ DSM) bali', '$2y$10$WlUspvBnyr8bTvnVQ1Fd..RR/0wHq3uiOBrOO.AT97cBmRMQwjHxW', 'lazdsm@zakat', 'Keputusan Dirjen Bimas Islam No. Dj. III/563 Tahun 2016 Tanggal 14 September 2016', NULL, 'Member', NULL, 'Jl. Diponegoro 157 Denpasar Bali', NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N');
INSERT INTO `users` VALUES (37, '37', NULL, 'active', '0', 'Yayasan Sinergi Foundation (LAZ Sinergi Foundation)', '$2y$10$WlUspvBnyr8bTvnVQ1Fd..RR/0wHq3uiOBrOO.AT97cBmRMQwjHxW', 'lazsf@zakat.com', 'Keputusan Dirjen Bimas Islam No.Dj.III/564 Tahun 2016 Tanggal 14 September 2016', NULL, 'Member', NULL, 'Jl. Sidamukti No. 99 H Bandung Jawa Barat', NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N');
INSERT INTO `users` VALUES (38, '38', NULL, 'active', '0', 'Yayasan Harapan Dhuafa Banten', '$2y$10$WlUspvBnyr8bTvnVQ1Fd..RR/0wHq3uiOBrOO.AT97cBmRMQwjHxW', 'lazdb@zakat.com', 'Keputusan Dirjen Bimas Islam No. Dj. III/651 Tahun 2016 Tanggal 27 Oktober 2016', NULL, 'Member', NULL, 'Jl. Ciawaru Raya Komplek Pondok Citra I Nomor IB Kota Serang Provinsi Banten', NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N');
INSERT INTO `users` VALUES (39, '39', NULL, 'active', '0', 'LAZ Yayasan Kesejahteraan Madani', '$2y$10$WlUspvBnyr8bTvnVQ1Fd..RR/0wHq3uiOBrOO.AT97cBmRMQwjHxW', 'lazkm@zakat.com', '', NULL, 'Member', NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N');
INSERT INTO `users` VALUES (40, '40', NULL, 'active', '0', 'LAZ Swadaya Ummah', '$2y$10$WlUspvBnyr8bTvnVQ1Fd..RR/0wHq3uiOBrOO.AT97cBmRMQwjHxW', 'lazsum@zakat.com', '', NULL, 'Member', NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N');
INSERT INTO `users` VALUES (41, '41', NULL, 'active', '0', 'LAZ Ibadurrahman', '$2y$10$WlUspvBnyr8bTvnVQ1Fd..RR/0wHq3uiOBrOO.AT97cBmRMQwjHxW', 'lazibad@zakat.com', '', NULL, 'Member', NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N');
INSERT INTO `users` VALUES (42, '42', NULL, 'active', '0', 'LAZ Abdurrahman Bin Auf', '$2y$10$WlUspvBnyr8bTvnVQ1Fd..RR/0wHq3uiOBrOO.AT97cBmRMQwjHxW', 'lazaba@zakat.com', '', NULL, 'Member', NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N');
INSERT INTO `users` VALUES (43, '43', NULL, 'active', '0', 'LAZ Komunitas Mata Air Jakarta', '$2y$10$WlUspvBnyr8bTvnVQ1Fd..RR/0wHq3uiOBrOO.AT97cBmRMQwjHxW', 'lazmaj@zakat.com', '', NULL, 'Member', NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N');
INSERT INTO `users` VALUES (44, '44', NULL, 'active', '0', 'LAZ Bina Insan Madani Dumai', '$2y$10$WlUspvBnyr8bTvnVQ1Fd..RR/0wHq3uiOBrOO.AT97cBmRMQwjHxW', 'lazbimd@zakat.com', '', NULL, 'Member', NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N');
INSERT INTO `users` VALUES (45, '45', NULL, 'active', '0', 'LAZ DSNI Amanah Batam', '$2y$10$WlUspvBnyr8bTvnVQ1Fd..RR/0wHq3uiOBrOO.AT97cBmRMQwjHxW', 'lazdsni@zakat.com', '', NULL, 'Member', NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N');
INSERT INTO `users` VALUES (46, '46', NULL, 'active', '0', 'LAZ rumah Peduli Umat Bandung Barat', '$2y$10$WlUspvBnyr8bTvnVQ1Fd..RR/0wHq3uiOBrOO.AT97cBmRMQwjHxW', 'lazpub@zakat.com', '', NULL, 'Member', NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N');
INSERT INTO `users` VALUES (47, '47', NULL, 'active', '0', 'LAZ Ummul Quro Jombang', '$2y$10$WlUspvBnyr8bTvnVQ1Fd..RR/0wHq3uiOBrOO.AT97cBmRMQwjHxW', 'lazuqj@zakat.com', '', NULL, 'Member', NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N');
INSERT INTO `users` VALUES (48, '48', NULL, 'active', '0', 'LAZ  lembaga Amil Zakat Nasional Baitul Mal Madinatul Iman', '$2y$10$WlUspvBnyr8bTvnVQ1Fd..RR/0wHq3uiOBrOO.AT97cBmRMQwjHxW', 'lazbmi@zakat.com', '', NULL, 'Member', NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N');
INSERT INTO `users` VALUES (49, '49', NULL, 'active', '0', 'LAZ Dompet Amanah Umat Sedati Sidoarjo', '$2y$10$WlUspvBnyr8bTvnVQ1Fd..RR/0wHq3uiOBrOO.AT97cBmRMQwjHxW', 'lazdaus@zakat.com', '', NULL, 'Member', NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N');
INSERT INTO `users` VALUES (50, '50', NULL, 'active', '0', 'Khoirul Anam', '$2y$10$WlUspvBnyr8bTvnVQ1Fd..RR/0wHq3uiOBrOO.AT97cBmRMQwjHxW', 'anam@fai-umj.ac.id', '', 'user.png', 'Member', '', 'Jl KH Ahmad dahlan Cirendeu CIputat Tangerang Selatan', '', NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '1', '50_syarat03_171108134033.pdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N');
INSERT INTO `users` VALUES (64, '0', NULL, 'active', '0', 'LAZ Tes', '$2y$10$8WFbtj3h2Qt0eSM5EtLxgOKwVfM67WasJW18uEsV5O8jFDwnKYu1y', 'laztes@gmail.com', NULL, NULL, 'BAZ', '085677778888', 'Gresik', NULL, 'laz.com', 0, 0, '-7.163849', '112.586983', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-08-26 23:51:46', '2020-08-26 23:52:40', 'N');
INSERT INTO `users` VALUES (67, '0', NULL, 'active', '0', 'akreditasi', '$2y$10$2zCpM7MJKvncay/Hl3.IGeUaXuWSD2NBfHbppvRmrTYS6dL5N9aNO', 'akreditasi@zakat.com', NULL, NULL, 'AKR', '123', 'test', NULL, 'test', 0, 0, '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-09-01 12:44:01', '2020-09-01 12:44:01', '');
INSERT INTO `users` VALUES (68, '0', NULL, 'active', '0', 'lembaga', '$2y$10$v3bpXJxZEGc8ICAhSpdDFuqB5rNdCFl8I0iqg0LSCFc5R3eEjU1R2', 'lembaga@zakat.com', NULL, NULL, 'LEM', '123', 'a', NULL, 'a', 0, 0, '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-09-01 12:44:54', '2020-09-01 12:44:54', '');

SET FOREIGN_KEY_CHECKS = 1;
