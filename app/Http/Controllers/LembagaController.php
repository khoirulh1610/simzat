<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\User;
class LembagaController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function verifikasi(){
    	$title = "Verifikasi";
    	$user = User::all();
    	return view('lembaga.verifikasi',compact('title','user'));
    }

    public function update(Request $request){
    	$id = $request->id;
    	$user = User::findorfail($id);
    	$user->verified = $request->verifikasi;
    	$user->save();
    	return redirect('admin/lembaga/verifikasi')->with('status', 'Data Buku Berhasil Ditambahkan');
    }
}
