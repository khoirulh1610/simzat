<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MstSyarat;
use App\User;
use DB,Auth;
use Validator;

class KandidatController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function profil(){
    	$title = "Profil";
    	return view('kandidat.profil',compact('title'));
    }

    public function document(){
    	$title = "Upload Document";
        $syarat  = MstSyarat::all();

        if(Auth::user()->user_type == 'admin'){
            $user = User::all();
        }else{
            $user = User::where('id', Auth::user()->id)->get();
        }
    	return view('kandidat.document',compact('title','syarat','user'));
    }

    public function syarat($id){
        $con = DB::connection('mysql');
        $data = $con->select("select * from mst_syarat");
        $isi = '';
        $i=1;
        foreach ($data as $col) {

            $isi .= '<tr>';
            $isi .= '<td align="center">'.$i.'</td>';
            $isi .= '<td>'.$col->nama.'</td>';
            $nm = $col->kode;
            $nmfile = $nm."_filename";

            $user = $con->select("select id,ifnull(".$nm.",0) as ".$nm.",ifnull(".$nmfile.",0) as ".$nmfile." from users where id like $id");

            if(isset($user[0]->$nm) && $user[0]->$nm==0){
              $isi .= '<td align="center"> <i class="fa fa-square-o" aria-hidden="true"></i></td>';
              $isi .= '<td align="center"> <i class="fa fa-check-square-o" aria-hidden="true"></i> </td>';
              $isi .= '<td width="100px"><a class="btn btn-info btn-block" name="'.$nm.'" id="'.$i.". ".$col->nama.'" onclick="OpenUpload(this.name,this.id)">Detail</a></td>';
            }elseif (isset($user[0]->$nm) && $user[0]->$nm==1) {
              $isi .= '<td align="center"> <a href="'.url('assets/uploads/pdf').'/'.$user[0]->$nmfile.'" target="_blank"><i class="fa fa-check-square-o" aria-hidden="true"></i></a> </td>';
              $isi .= '<td align="center"> <i class="fa fa-square-o" aria-hidden="true"></i></td>';
              $isi .= '<td width="100px"><a class="btn btn-info btn-block" name="'.$nm.'" id="'.$i.". ".$col->nama.'" onclick="OpenUpload(this.name,this.id)">Detail</a></td>';
            }else {
              $isi .= '<td align="center"> <i class="fa fa-square-o" aria-hidden="true"></i> </td>';
              $isi .= '<td align="center"> <i class="fa fa-square-o" aria-hidden="true"></i></td>';
              $isi .= '<td width="100px"><input type="button" class="btn btn-info btn-block" name="" onchange="OpenUpload(this.name);" value="Detail" disabled/></td>';
            }
            $isi .= '</tr>';
            $i++;

        }
        echo $isi;
    }


    public function url_pdf($id)
    {
        $con = DB::connection('mysql');
        $sql = "select * from users where id like '".$id."'";

        $data = count($con->select($sql));
        $res = $con->select($sql);
        $nm = $_POST['syarat']."_filename";


        if(($data==1) && ($res[0]->$nm!='')){
            echo '<a href="'.url('assets/uploads/pdf').'/'.$res[0]->$nm.'" class="btn btn-success" target="_blank">Download</a>&nbsp;';
            echo '<button  onclick="del_pdf()" class="btn btn-success">Delete</button>';
        }else {
            echo '';
        }
    }


    public function del_pdf(Request $request)
    {
        $user_id = $request->input('user_id');
        $syarat = $request->input('syarat');
        $file_name = $syarat."_filename";
        $con = DB::connection('mysql');
        $sql = "update users set $syarat=null,$file_name=null where id like $user_id";
        if($con->Select($sql)){
            echo "Delete successfully";
        }
    }

    function upload_file(Request $request) {
        
        $validator = Validator::make($request->all(), [            
            'filepdf' => 'required|mimetypes:application/pdf|max:10000'
          ]);
          if ($validator->passes()) {
            $input = $request->all();            
            $input['filepdf'] = $request->user_id.'_'.$request->syarat.'_'.time().'.'.$request->filepdf->getClientOriginalExtension();
            $request->filepdf->move(public_path('/data'),$input['filepdf']);
    
            // Upload_file::create($input);
            // return $request->all();
            $user = DB::table('users')->where('id',$request->user_id)->update([$request->syarat=>1,$request->syarat."_filename"=>$input['filepdf']]);


            // return response()->json(['success'=>'Berhasil']);
          }
    
          return response()->json(['error'=>$validator->errors()->all()]);
    }




    // distribusi
    public function getdist(Request $request)
    {
        $title = "Perolehan & Distribusi";
        if(Auth::user()->user_type == 'admin'){
            $user = User::all();
        }else{
            $user = User::where('id', Auth::user()->id)->get();
        }
        return view('kandidat.dist',compact('title','user'));
    }


    public function tbgetdist($id='')
    {
        $con = DB::connection('mysql');
        $data = $con->select("select * from tbl_nerca where user_id='$id' order by STR_TO_DATE(concat('01-',periode),'%d-%M-%Y')");
        $isi = '';
        $i=1;
        if(count($data) <> 0){
            foreach ($data as $col) {
                $isi .= '<tr>';
                $isi .= '<td align="center">'.$col->periode.'</td>';
                $isi .= '<td align="right">'.number_format($col->zakat,2).'</td>';
                $isi .= '<td align="right">'.number_format($col->infaq,2).'</td>';
                $isi .= '<td align="right">'.number_format($col->dansos_lain,2).'</td>';
                $isi .= '<td align="right">'.number_format($col->total_peroleh,2).'</td>';
                $isi .= '<td align="right">'.number_format($col->konsumtif,2).'</td>';
                $isi .= '<td align="right">'.number_format($col->produktif,2).'</td>';
                $isi .= '<td align="right">'.number_format($col->total_distribusi,2).'</td>';
                $isi .= '<td align="right">'.number_format($col->saldo,2).'</td>';
                $isi .= '<td width="10px" align="center"> 
                            <a class="fa fa-pencil" onclick="showedit('.$col->id.')"><a/>
                            &nbsp;&nbsp;&nbsp;
                            <a class="fa fa-remove" onclick="showdel('.$col->id.')"><a/></td>';
                // $isi .= '<td width="10px" align="center"> </a> <a class="fa fa-remove"></a> </td>';
                $isi .= '<tr>';
            }
        }else{
            $isi .= "<tr><td colspan='10' align='center'>Data masih belum dipilih</td></tr>";
        }
        echo $isi;

    }


    public function getdistapi(Request $request){
        $id = $request->id;
        $act = $request->act;
        $data = $request;
        $con = DB::connection('mysql');

        if ($act=='insert') {
            $cek = $con->select("select * from tbl_nerca where user_id like '".$data->user_id."' 
                    and periode like '".$data->periode."'");
            if(count($cek)==0){
                
                $insert = $con->select("insert into tbl_nerca(user_id,periode,zakat,infaq,dansos_lain,total_peroleh,konsumtif,produktif,total_distribusi,saldo) values('".$data->user_id."','".$data->periode."','".$data->zakat."', '".$data->infaq."','".$data->dansos_lain."','".$data->total_peroleh."','".$data->konsumtif."','".$data->produktif."','".$data->total_distribusi."','".$data->saldo."')");

                $results = array('status' => 'Success','error'=>'NO','msg'=>'');
                
            }else {
                $results = array('status' => 'Gagal','error'=>'YES','msg'=>'This data already Exist');
            }

            return $results;

        }elseif ($act=='update') {

            $cek = $con->Select("select * from tbl_nerca where id = $id");
            if(count($cek)!=0){
                $update = $con->select("update tbl_nerca set periode='".$request->periode."',zakat='".$request->zakat."',infaq='".$request->infaq."',dansos_lain='".$request->dansos_lain."',total_peroleh='".$request->total_peroleh."',konsumtif='".$request->konsumtif."',produktif='".$request->produktif."',total_distribusi='".$request->total_distribusi."',saldo='".$request->saldo."' where id='$id'");
                $results = array("status"=>"Success",'error'=>'NO',"msg"=>"Data berhasil diupdate");
            }else{
                $results = array("status"=>"ERROR",'error'=>'YES',"msg"=>"Value not Set");
            }


        }else {
              $results = array("status"=>"ERROR",'error'=>'YES',"msg"=>"Value not Set");
        }

        return $results;
    }


    public function deletenrc($id){
        $con = DB::connection('mysql');
        $cek = $con->select("select * from tbl_nerca where id = $id");
        if(count($cek)!=0){
            $qry = $con->select("delete from tbl_nerca where id like $id");
            $results = array("status"=>"Success",'error'=>'NO',"msg"=>"Data berhasil dihapus");
        }else {
          $results = array("status"=>"ERROR",'error'=>'YES',"msg"=>"Value not Set");
        }
        return $results;
    }


    public function shownrc($id){
        $con = DB::connection('mysql');
        $results = array('status' => 'Success','Error'=>'NO','Msg'=>'');
        $results['data'] = $con->select("select * from tbl_nerca where id='$id'");

        return $results;
    }
}
