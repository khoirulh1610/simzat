<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pengaduan;
class PengaduanController extends Controller
{

	 public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $title = "Pengaduan Masyarakat";
        $pengaduan = Pengaduan::all();
        return view('admin.aduan.home',compact('title','pengaduan'));
    }

}
