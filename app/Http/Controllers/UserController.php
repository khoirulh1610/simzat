<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\MenuGroup;
use App\MenuUser;
use DB;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $title = "Kelola Users";
        $data = User::all();
        return view('admin.user.home', compact('title','data','group','menu'));
    }

    public function loaduser(){
        $data = User::all();
        $isi = '';
        $i=1;
        foreach ($data as $col) {
            $id = $col->id;
            $isi .= "<tr>";

            $isi .= "<td>$i</td>";
            $isi .= "<td>$col->name</td>";
            $isi .= "<td>$col->email</td>";
            $isi .= "<td>$col->user_type</td>";
            $isi .= "<td>$col->no_hp</td>";
            $isi .= "<td>$col->status</td>";
            $isi .= "<td>$col->lat</td>";
            $isi .= "<td>$col->lng</td>";
            $isi .= "<td>
                            <a href='#' class='btn btn-xs btn-info' onClick='showedit($id)'><i class='fa fa-edit'></i></a> 
                            <a href='#' class='btn btn-xs btn-danger' onClick='showdel($id)'><i class='fa fa-trash'></i></a>
                    </td>";
            
            $isi .= '</tr>';
            $i++;

        }
        return $isi;
    }



    public function store(Request $request){
        $id = $request->id;
        $act = $request->act;
        if ($act=='insert') {
            $user = new User();
            $user->name = $request->nama;
            $user->email = $request->email;
            $user->no_hp = $request->telp;
            $user->user_type = $request->type;
            $user->status = $request->status;
            $user->lat = $request->lat;
            $user->lng = $request->lng;
            $user->alamat = $request->alamat;
            $user->website = $request->website;
            $user->password = bcrypt($request->password);

            $con = DB::connection('mysql');
            $cek = $con->select("select * from users where email = '".$request->email."'");
            if(count($cek)==0){
                $user->save();
                $results = array("status"=>"Success",'error'=>'NO',"msg"=>"Data berhasil disimpan");
            }else {
                $results = array("status"=>"ERROR",'error'=>'YES',"msg"=>"Email sudah terdaftar");
            }

        }elseif ($act=='update') {
            $user = User::findorfail($id);
            $user->name = $request->nama;
            $user->email = $request->email;
            $user->no_hp = $request->telp;
            $user->user_type = $request->type;
            $user->status = $request->status;
            $user->lat = $request->lat;
            $user->lng = $request->lng;
            $user->alamat = $request->alamat;
            $user->website = $request->website;

            if($request->password <> ''){
                $user->password = bcrypt($request->password);
            }
            $user->save();
            $results = array("status"=>"Success",'error'=>'NO',"msg"=>"Data berhasil diupdate");
        }
        return $results;
    }


    public function delete($id){
        $con = DB::connection('mysql');
        $cek = $con->select("select * from users where id = $id");
        if(count($cek)!=0){
            $qry = $con->select("delete from users where id like $id");
            $results = array("status"=>"Success",'error'=>'NO',"msg"=>"Data berhasil dihapus");
        }else {
          $results = array("status"=>"ERROR",'error'=>'YES',"msg"=>"Value not Set");
        }
        return $results;
    }

    public function show($id){
        $con = DB::connection('mysql');
        $results = array('status' => 'Success','Error'=>'NO','Msg'=>'');
        $results['data'] = $con->select("select * from users where id='$id'");
        return $results;
    }

    public function showgroup($id){
        $group = MenuGroup::where('user_id',$id)->get();
        return $group;
    }

    public function tambahAkses(Request $request){
        if(empty($request->group_id)){
            $group  = new MenuGroup();
            $group->user_id = $request->user_id;
            $group->group = $request->group;
            $group->save();

            $user = User::findorfail($request->user_id);
            $user->group_id = $request->group_id;
            $user->save();
            echo "Group telah ditambahkan";
            exit;
        }
       
        $cek = MenuUser::where('group_id',$request->group_id)->where('menu',$request->menu)->get();
        if($cek->count() > 0){
            $menu = MenuUser::findorfail($cek[0]->id);
            // echo "Data berhasil di update";
        }else{
            // echo "Data berhasil ditambahkan";
            $menu = new MenuUser();
        }
        
        $menu->group_id = $request->group_id;
        $menu->menu = $request->menu;
        $menu->save();
        echo $request->group_id;
    }


    function AksesData($id){
        $data = MenuUser::where('group_id',$id)->get();
        $isi = '';
        $i=1;
        if($data->count() > 0){
            foreach ($data as $col) {
                $group = MenuGroup::findorfail($col->group_id);
                $user = User::findorfail($group->user_id);
                $isi .= '<tr>';
                $isi .= '<td align="center">'.$i.'</td>';
                $isi .= '<td>'.$user->name.'</td>';
                $isi .= '<td>'.$user->user_type.'</td>';
                $isi .= '<td>'.$group->group.'</td>';
                $isi .= '<td>'.$col->menu.'</td>';
                $isi .= '<td><a href="#" onClick="aksesHapus('.$col->id.','.$col->group_id.')" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> </a></td>';
                $isi .= "</tr>";
                $i++;
            }
        }else{
            $isi .= "<tr><td colspan='6'>Data tidak ditemukan</td></tr>";
        }
        echo $isi;
    }


    function aksesHapus($id){
        $menu = MenuUser::findorfail($id);
        echo $menu->group_id;
        $menu->delete();

    }
}
