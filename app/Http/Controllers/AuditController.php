<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use DB;

class AuditController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
    }

    public function import(){
    	$title = "Import Data LPZ";
    	$user = User::all();
    	return view('audit.importdatalaz',compact('title','user'));
    }




    public function kinerja(){
    	$title = "Kinerja Keuangan";
    	return view('audit.kinerja',compact('title'));
    }




    public function kinerja_nilai($id){
    	$title = "Kinerja Keuangan";
    	return view('audit.kinerja_nilai',compact('title','id'));
    }



    public function postkinerja(Request $request){
    	$user_id = $request->user_id;
      	$kin_no = $request->kin_no;
		$kin_subno = $request->kin_subno;
		
		
      	$nilai = $request->nilai;
      	$con = DB::connection('mysql');
      	$kin_val = '';
      	if($nilai==4){
        	$kin_val = 'A';
      	}elseif ($nilai==3) {
        	$kin_val = 'B';
      	}elseif ($nilai==2) {
        	$kin_val = 'C';
      	}elseif ($nilai==1) {
        	$kin_val = 'D';
      	}else {
        	$kin_val = 'E';
      	}
      	$sql = "select * from lpz_kinerja where user_id = '$user_id' and kin_no = '$kin_no' and kin_subno = '$kin_subno'";
      	$cnt = $con->select($sql);
      	if(count($cnt)==1){
			echo "update";
			echo "update lpz_kinerja set kin_val='$kin_val', kin_skor='$nilai' where user_id = '$user_id' and kin_no = '$kin_no' and kin_subno = '$kin_subno'";
        	$con->Select("update lpz_kinerja set kin_val='$kin_val', kin_skor='$nilai' where user_id = '$user_id' and kin_no = '$kin_no' and kin_subno = '$kin_subno'");
      	}else{
			echo "insert";
        	$con->select("insert into lpz_kinerja(user_id,kin_no,kin_subno,kin_val,kin_skor)values('$user_id','$kin_no','$kin_subno','$kin_val','$nilai')");
      	}
    }



    public function nilaikinerja(Request $request){
      $user = $request->user_id;
      $con = DB::connection('mysql');
      $sql = "select sum(a.kin_skor * b.kin_bobot) skor from lpz_kinerja a LEFT JOIN mst_kinerja b ON (a.kin_no=b.kin_no and a.kin_subno=b.kin_subno) where a.user_id = '$user'";
      $cnt = $con->select($sql);
      if(count($cnt)==1){
        $data = $con->select($sql);
        echo $data[0]->skor;
      }else {
        echo 0;
      }
    }




    public function kepatuhan(){
    	$title = "Penilaian Kepatuhan Syariah";
    	return view('audit.kepatuhan',compact('title'));
    }



    public function kepatuhan_nilai($id){
    	$title = "Penilaian Kepatuhan Syariah";
    	return view('audit.kepatuhan_nilai',compact('title','id'));
    }


    public function postnilai(Request $request){
    	$con = DB::connection('mysql');
      	$user_id = $request->user_id;
      	$patuh_subno = $request->patuh_subno;
      	$patuh_val = $request->patuh_val;
      	$patuh_skor = $request->patuh_skor;
      	$sql = "select * from lpz_kepatuhan where user_id = '$user_id' and patuh_subno = '$patuh_subno'";
      	$cnt = $con->select($sql);
      	if(count($cnt)==1){
        	$con->select("update lpz_kepatuhan set patuh_val='$patuh_val', patuh_skor='$patuh_skor' where user_id = '$user_id' and patuh_subno = '$patuh_subno'");
      	}else{
        	$con->select("insert into lpz_kepatuhan(user_id,patuh_subno,patuh_val,patuh_skor)values('$user_id','$patuh_subno','$patuh_val','$patuh_skor')");
      	}
    }


    public function getsubnilai(Request $request){
    	$con = DB::connection('mysql');
      	$user = $request->user_id;
      	$sub = $request->sub;
      	$sql = "select sum(patuh_skor) as sumskor from lpz_kepatuhan where user_id = '$user' and substr(patuh_subno,1,locate('.',patuh_subno)-1) = '$sub' GROUP BY id,substr(patuh_subno,1,locate('.',patuh_subno)-1)";
      	$cnt = $con->select($sql);
      	if(count($cnt)==1){
        	$data = $con->select($sql);
        	echo $data[0]->sumskor;
      	}else {
        	echo 0;
      	}
    }
}
