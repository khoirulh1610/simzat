<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\User;
class LandingController extends Controller
{


	public function index(Request $request){		
		$baznas = User::where('user_type','BAZ')->paginate(3);
		$laz = User::where('user_type','LAZ')->paginate(3);
		$lain = User::where('user_type','BBI')->paginate(3);
		$tab =  "tab1";
		$filter = $request->filter;
		if($request->filter){
			$baznas = User::where('user_type','BAZ')->where('name','like','%'.$request->filter.'%')->paginate(3);
			$laz = User::where('user_type','LAZ')->where('name','like','%'.$request->filter.'%')->paginate(3);
			$lain = User::where('user_type','BBI','LAZ')->where('name','like','%'.$request->filter.'%')->paginate(3);
			$tab = $request->tab ?? 'tab1';
		}
		return view('welcome',compact('baznas','laz','lain','tab','filter'));
	}


    
    public function PengaduanStore(Request $request){
    	 // untuk validasi form
	    $this->validate($request, [
	        'nama' => 'required',
	        'email' => 'required',
	        'no_hp' => 'required',
	        'keterangan' => 'required'
	    ]);
	    // insert data ke table books
	    DB::table('pengaduan')-> insert([
	        'nama' => $request->nama,
	        'email' => $request->email,
	        'no_hp' => $request->no_hp,
	        'keterangan' => $request->keterangan,
	        'created_at'=> date('Y-m-d H:i:s'),
	        'updated_at'=> date('Y-m-d H:i:s')
	    ]);
	    // alihkan halaman tambah buku ke halaman books
	    // return Redirect::back()->with('msg', 'The Message');
	    return redirect('/')->with('status', 'Data Buku Berhasil Ditambahkan');
	}
	

	public function pencarian(Request $request){
		$cari = "Y";
		return view('welcome',compact('cari'));
	}
}
